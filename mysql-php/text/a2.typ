#import "/tpl.typ": *
#show: A => apply(A)
#set raw(lang: "php")
#set par(leading: 0.65em)

#assignment(2)[
  Design a `Login` table to take `username` and `password`, and
  - Show all records of the `Student` table for an authorized user, whose username and password exists in the `Login` table.
  - Allow the authenticated users to change their username and password.
]

#scos(2)[
  #{ /* image("/output/2.png", width: 100%)*/ []}
  === Login form
  #image("/output/2/1.png", width: 80%)
  === Display records on success
  #image("/output/2/2.png", width: 90%)
]

#signature()
