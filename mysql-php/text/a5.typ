#import "@preview/tablex:0.0.6": tablex, vlinex, hlinex
#import "/tpl.typ": *
#show: A => apply(A)
#set raw(lang: "php")
#set par(leading: 0.7em)

#assignment(5)[
  Design a HTML form to take inputs from the user and store the records into a table `Employee` using a PHP script. The form should contain the following fields:

  #align(center, tablex(
    columns: 2,
    auto-lines: false,
    hlinex(),
    vlinex(), vlinex(), vlinex(),
    [*Fields*], [*Form input control*],
    hlinex(),
    [`Ename`], [textbox],
    [`Address`], [textarea],
    [`Phno`], [textbox],
    [`Salary`], [textbox],
    [`Category` \ (out of GEN, SC, ST, OBC)], [radio buttons],
    [`Language`], [checkbox],
    hlinex(),
  ))
  (Multiple languages have to be concatenated into a string separated by commas and then stored into the database like `Bengali, English, Hindi`.)

  Once the form is submitted, the PHP script should process the input data and insert a new record into the `Employee` table in the database. Ensure that appropriate validation is implemented to handle user input correctly.

  Additionally, implement functionality to display the employee records from the `Employee` table. The records should be ordered based on the user's choice of field, which can be selected from a dropdown menu containing all fields of the table. Optionally, provide a checkbox to allow the user to specify descending order for the displayed records.
]

#scos(5, cont: true)[
  === Menu
  #align(center, image("/output/5/1.png", width: 20%))
  === Input Form
  #align(center, image("/output/5/2.png", width: 30%))
  === Display records
  #align(center, image("/output/5/3.png", width: 90%))
  #align(center, image("/output/5/4.png", width: 90%))
]

#signature()
