#import "/tpl.typ": *
#show: A => apply(A)
#set raw(lang: "php")
#set par(leading: 0.725em)

#assignment(1)[
  Create a table `Student` in a MySQL database containing the fields `name`, `roll`, `city`, `email` and `date_of_birth` and insert some records into this table and display them using PHP.
]

#scos(1)[
  === Menu page
  #align(center, image("/output/1/1.png", width: 80%))
  === Form page
  #align(center, image("/output/1/2.png", width: 70%))
  #v(4em)
  === Display record
  #image("/output/1/3.png", width: 80%)
  === Display record (2000--2005)
  #image("/output/1/4.png", width: 80%)
  === Search record by email
  ==== Form
  #align(center, image("/output/1/5.png", width: 50%))
  #v(3em)
  ==== Record found
  #align(center, image("/output/1/6.png", width: 70%))
  ==== Record not found
  #align(center, image("/output/1/7.png", width: 80%))
]

#signature()
