#import "/tpl.typ": *
#show: A => apply(A)
#set raw(lang: "php")
#set par(leading: 0.7em)

#assignment(4)[
  Create an HTML form to input employee salary components such as Basic Pay, House Rent Allowance (HRA) percentage, Dearness Allowance (DA) percentage, and Professional Tax. Upon submission, the PHP script should calculate the HRA, DA, and Professional Tax for each employee based on the provided percentages and store these details in a database table named `Salary`. Ensure that records in the `Salary` table contain entries only for employee names and basic pay. Finally, display the stored salary records including employee names, basic pay, calculated HRA, DA, and Professional Tax.
]

#scos(4, cont: true)[
  === Login form
  #image("/output/4/1.png", width: 60%)
  === Display records
  #image("/output/4/2.png", width: 80%)
]

#signature()
