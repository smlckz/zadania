#import "/tpl.typ": *
#show: A => apply(A)
#set raw(lang: "php")
#set par(leading: 0.7em)

#assignment(3)[
  Create the following tables: `Food_Details` with the fields `Food_item` and `Price_per_item`; `Customer_details` with the fields `customer_name`, `total_amount_paid`, `date_of_payment`. Design a form with the menu of food items, taking order from customer. Generate a bill containing the customer name, food items, quantity, and total price and insert a record in the `Customer_details` table. Net price is the total price plus 15% GST of total price.
]

#scos(3)[
  === Menu form to order food items
  #align(center, image("/output/3/1.png", width: 65%))
  === Bill generated from an order
  #align(center, image("/output/3/2.png", width: 70%))
]

#signature()
