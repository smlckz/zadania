#import "@preview/codelst:1.0.0": sourcefile

/* Highlights the source code file. */
#let highlight-code-file(filename) = sourcefile(read(filename), file: filename)

/* The state variable to indicate whether the end of an assignment is reached. */
#let eoa = state("is-at-end-of-assignment", false)

/* Updates the state variable to indicate the end of an assignment. */
#let signature() = {
  eoa.update(true)
}

/* Draws the signature construct at the bottom right corner in the footer of the last page of an assignment. */
#let signature-footer(loc) = {
  if eoa.at(loc) {
    align(bottom + right,
      move(dy: -4em, dx: -1em,
        block(width: 15em)[
          #v(3em)
          #line(length: 100%) \
          #v(-2.5em)
          #align(center)[Teacher’s signature]
        ]))
    eoa.update(false)
  }
}

/* Draws page border around the provided content, taking an optional function to be called at the footer. */
#let apply-page-borders(body, font-options: (), footer-special-func: none, page-numbering: none) = {
  let page-margin = (left: 0.75in, right: 0.25in, top: 0.25in, bottom: 0.25in)
  let margin = (left: 0.65in, right: 0.15in, top: 1.5em, bottom: 1.5em)
  let page-border-thickness = 1.25pt
  set page(
    margin: (..page-margin, bottom: margin.bottom + 2em),
    numbering: if page-numbering != none { page-numbering } else { "1" },
    background: align(top + start, pad(..margin, rect(width: 100%, height: 100%, stroke: page-border-thickness + gray, radius: 5pt))),
    footer: locate(loc => {
      align(center, move(dy: -margin.bottom + 1em, text(..font-options, size: 9pt, counter(page).display(loc.page-numbering()))))
      if footer-special-func != none {
        footer-special-func(loc)
      }
    })
  )
  show: block.with(breakable: true, width: 100%, inset: page-border-thickness + 1em) 
  body
}

#let apply(body, page-numbering: none) = {
  let body-font-settings = (font: "Nunito Sans 10pt", size: 12pt, stretch: 75%)
  set text(..body-font-settings)
  show raw: set text(font: "Iosevka Fixed", size: 1.1em)
  show raw.where(block: false): box.with(
    inset: (x: 3pt, y: 0pt),
    outset: (y: 3pt),
    radius: 2pt,
  )
  show raw.where(block: true): block.with(
    inset: 10pt,
    radius: 4pt,
    width: 100%,
  )
  show raw.where(block: true): it => align(left, it)
  set raw(theme: "vendor/gr.tmTheme")
  set list(marker: ([$square.filled.tiny$], [--]))
  set par(leading: 0.5em)
  apply-page-borders(body, font-options: body-font-settings, footer-special-func: signature-footer, page-numbering: page-numbering)
}


#let alist = state("assignment-list", ())

#let list-of-assignments(contents) = locate(loc => {
  let assignment-list = alist.final(loc)
  let last-page-number = counter(page).final(loc).first()
  contents(assignment-list, last-page-number)
})

#let list-of-dates = ([11/09/2023], [16/09/2023], [26/09/2023], [04/10/2023], [09/10/2023], [10/10/2023], [13/10/2023], [16/10/2023], [21/11/2023], [28/11/2023], [02/12/2023], [04/12/2023], [04/12/2023], [05/12/2023], [19/12/2023], [02/01/2024], [08/01/2024], [09/01/2024], [15/01/2024], [18/01/2024])
#let assignment-dates-indices = (15, 16, 17, 18, 19)

#let assignment(number, description, reduce-gap: false, pad: false) = align(center, [
= #text(weight: 600, [Assignment #number])
  #{
    let date = list-of-dates.at(assignment-dates-indices.at(number - 1) - 1)
    v(-1.65em)
    align(right, [Date: #date])
    if reduce-gap { v(-0.75em) }
    align(left)[
      #set par(justify: true)
== #text(weight: 500, [Program statement:]) #text(weight: 400, description)
    ]
    locate(loc => alist.update(lst => (..lst, (number: number, description: description, page-number: counter(page).at(loc).first(), date: date))))
  }
])

#let scos(n, obody, cont: false) = [
  === Source Code
  #show raw: set text(size: 10pt)
  #highlight-code-file("/code/a" + str(n) + ".php")
  #if cont == true [ #colbreak() ]
  === Output
  #obody
]

