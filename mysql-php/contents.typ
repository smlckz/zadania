#import "@preview/tablex:0.0.7": tablex, cellx
#import "/tpl.typ": apply, signature, list-of-assignments, apply-page-borders

#apply(page-numbering: "(i)" , [
  #let heading-format(content) = cellx(align: center + horizon, content)
  #let column-alignments = (right, auto, center + horizon, center + horizon, auto)
  #let preprocess-alist(assignment-list, last-page-number) = {
    let index = 0
    let last-index = assignment-list.len() - 1
    let page-number-list = ()
    while index < last-index {
      let item = assignment-list.at(index)
      let next-item = assignment-list.at(index + 1)
      let starting-page-number = item.page-number
      let finishing-page-number = next-item.page-number - 1
      page-number-list.push((starting-page-number, finishing-page-number))
      index = index + 1
    }
    page-number-list.push((assignment-list.at(last-index).page-number, last-page-number))
    let new-assignment-list = ()
    index = 0
    for (start, end) in page-number-list {
      let page-number = if start == end [#start] else [#start - #end]
      let assignment = assignment-list.at(index)
      let serial-number = [#{assignment.number}. ]
      let description = stack(dir: ltr, v(5em), assignment.description)
      let item = (serial-number, description, page-number, assignment.date, [])
      new-assignment-list.push(item)
      index = index + 1
    }
    new-assignment-list
  }
  #list-of-assignments((assignment-list, last-page-number) => {
    counter(page).update(1)
    align(center, [== Contents])
    tablex(
      columns: (3em, 1fr, 4em, 6em, 11em),
      stroke: 1pt + gray,
      repeat-header: true,
      map-cols: (i, cells) => (cells.first(), ..cells.slice(1).map(cell => (..cell, align: column-alignments.at(i)))),
      heading-format[*Sl.* \ *No.*], heading-format[*Description*], heading-format[*Page No.*], heading-format[*Date*], heading-format[*Teacher’s* \ *Signature*],
      ..preprocess-alist(assignment-list, last-page-number).flatten(),
    )
    // signature()
  })
])

#colbreak()

