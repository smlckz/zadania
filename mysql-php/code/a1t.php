<?php
declare(strict_types=1);
error_reporting(E_ALL);
// student: name roll city email date_of_birth
function connect_to_database() {
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    if (!($dbh = mysqli_connect('localhost', 'root', '')))
        display_failure('Could not connect to the database: ' . mysqli_connect_error($dbh));
    mysqli_set_charset($dbh, 'utf8mb4');
    if (!mysqli_query($dbh, 'CREATE DATABASE IF NOT EXISTS STUDENTS_DB'))
        display_failure('Could not create database: ' . mysqli_error($dbh));
    mysqli_select_db($dbh, 'STUDENTS_DB');
    if (!mysqli_query($dbh, 'CREATE TABLE IF NOT EXISTS STUDENT (
        ROLL INT(20) PRIMARY KEY,
        NAME VARCHAR(255),
        CITY VARCHAR(255),
        EMAIL VARCHAR(255),
        DATE_OF_BIRTH DATE
    )'))
        display_failure('Could not create table: ' . mysqli_error($dbh));
    return $dbh;
}

// Generate the Typst table for records
function show_table($result) { ?>
#table(
    columns: 5,
    [*`ROLL`*], [*`NAME`*], [*`CITY`*], [*`EMAIL`*], [*`DATE_OF_BIRTH`*],
<?php while ($row = mysqli_fetch_assoc($result)) { ?>
    <?php echo '[`' . implode('`], [`', array_map('htmlspecialchars', [$row['ROLL'], $row['NAME'], $row['CITY'], $row['EMAIL'], $row['DATE_OF_BIRTH']])) . '`], ' . "\n"; ?>
<?php } ?>
)
<?php }

function show_table_normal($dbh) {
    if(!($result = mysqli_query($dbh, 'SELECT * FROM STUDENT')))
        display_failure('Could not perform query: ' . mysqli_error($dbh));
    show_table($result);
}

$dbh = connect_to_database();
show_table_normal($dbh);
mysqli_close($dbh);

