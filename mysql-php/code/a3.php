<?php
declare(strict_types=1);
error_reporting(E_ALL);

function connect_to_database() {
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $dbh = new mysqli('localhost', 'root', '');
    $dbh->set_charset('utf8mb4');
    $dbh->query('CREATE DATABASE IF NOT EXISTS EATERY_DB');
    $dbh->select_db('EATERY_DB');
    $dbh->query('CREATE TABLE IF NOT EXISTS `FOOD_DETAILS` (`FOOD_ID` INT PRIMARY KEY, `FOOD_ITEM` VARCHAR(255), `PRICE_PER_ITEM` DOUBLE, UNIQUE (`FOOD_ID`))');
    $dbh->query('INSERT INTO `FOOD_DETAILS` VALUES
        (1, "Egg Biriyani (w/ 2 eggs)", 200),
        (2, "Chicken Biriyani", 300),
        (3, "Mutton Biriyani", 350),
        (4, "Veg Pulao", 200),
        (5, "Shahi Paneer", 220),
        (6, "Schezwan Paneer", 240),
        (7, "Steam Rice", 90)
        ON DUPLICATE KEY UPDATE `FOOD_ITEM` = VALUES(`FOOD_ITEM`)');
    $dbh->query('CREATE TABLE IF NOT EXISTS `CUSTOMER_DETAILS` (`BILL_ID` INT PRIMARY KEY AUTO_INCREMENT, `CUSTOMER_NAME` VARCHAR(255), `TOTAL_AMOUNT_PAID` DOUBLE, `DATE_OF_PAYMENT` DATE)');
    return $dbh;
}

function html_prologue($title) {
?><!doctype html>
<meta charset="utf-8">
<title><?php echo $title; ?></title>
<style>
body { font-family: sans-serif; font-size: 1.3rem; }
h1 { font-size: 2rem; font-weight: 500; }
h2 { font-size: 1.8rem; font-weight: 500; }
form { margin: 2em auto; width: 20em; }
form > * { padding: 0.5em; }
table, tr, th, td { border-collapse: collapse; border: 1px solid black; }
table.noborder, table.noborder * { border: none; }
table.invoice td:nth-child(2), table.invoice td:nth-child(3) { text-align: end; }
p.right { text-align: right; }
th, td { padding: 5px; }
</style>
<?php
}

function display_failure($reason) {
    html_prologue('Operation failure');
    ?>
<h2>Operation failed</h2>
<p>Reason: <?php echo $reason; ?></p>
<?php
    die();
}

function show_menu($dbh) {
    html_prologue('Eatery Menu');
    ?>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<h1>Welcome to Delish Eatery</h1>
<table class="noborder">
<tr><td><label for="name">Name:</label></td><td colspan="2"><input type="text" name="customer_name" id="name"></td></tr>
<tr><th>Servings</th><th>Food item</th><th>Price</th></tr>
    <?php $result = $dbh->query('SELECT * FROM `FOOD_DETAILS`');
    while ($row = $result->fetch_assoc()) {
        echo '<tr><td><input type="number" min="0" placeholder="0" name="servings[', $row['FOOD_ID'], ']"></td>';
        echo '<td>', $row['FOOD_ITEM'], '</td>', '<td>₹', number_format(+$row['PRICE_PER_ITEM'], 2), '</td></tr>';
    } ?>
<tr><td colspan="3"><input type="submit" value="Order"></td></tr>
</table>
</form>
    <?php
}

function process_order($dbh) {
    if (empty($_POST['customer_name']))
        display_failure('Can not order without the customer name');
    $servings = empty($_POST['servings']) ? [] : array_filter(array_map('intval', $_POST['servings']));
    if (count($servings) == 0)
        display_failure('No serving selected in the order');
    $orders = array_filter(array_map('intval', array_keys($servings)));
    $items = $dbh->query('SELECT * FROM `FOOD_DETAILS` WHERE `FOOD_ID` IN (' . implode(', ', $orders) . ')')->fetch_all(MYSQLI_ASSOC);
    $total_price = 0.0;
    foreach ($items as $i => $item) {
        $items[$i]['price'] = $servings[$item['FOOD_ID']] * $item['PRICE_PER_ITEM'];
        $total_price += $items[$i]['price'];
    }
    $tax = 0.15 * $total_price;
    $net_price = $total_price + $tax;
    $stmt = $dbh->prepare('INSERT INTO `CUSTOMER_DETAILS` (`CUSTOMER_NAME`, `TOTAL_AMOUNT_PAID`, `DATE_OF_PAYMENT`) VALUES (?, ?, ?)');
    $stmt->bind_param('sds', $_POST['customer_name'], $net_price, @date('Y-m-d'));
    $stmt->execute();
    $bill_id = $dbh->insert_id;
    html_prologue('Customer invoice');
    ?>
<h1>Customer Invoice</h1>
<p class="right">Bill No.: D.E./<?php echo date('Y.m.d'), '/', $bill_id; ?></p>
<p><b>Name</b>: <?php echo $_POST['customer_name']; ?></p>
<table class="invoice">
<tr><th>Food item</th><th>Servings</th><th>Price</th></tr>
<?php foreach ($items as $item) {
    echo '<tr><td>', implode('</td><td>', [$item['FOOD_ITEM'], $servings[$item['FOOD_ID']], number_format($item['price'], 2)]), '</td></tr>';
} ?>
<tr><td colspan="2">Total:</td><td><?php echo number_format($total_price, 2); ?></td></tr>
<tr><td colspan="2">GST (15%):</td><td><?php echo number_format($tax, 2); ?></td></tr>
<tr><td colspan="2">Net price:</td><td><?php echo number_format($net_price, 2); ?></td></tr>
</table>
    <?php
}

try {
    $dbh = connect_to_database();
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
        process_order($dbh);
    else show_menu($dbh);
    $dbh->close();
} catch (mysqli_sql_exception $e) {
    display_failure($e->getMessage());
}

