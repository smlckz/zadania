#import "@preview/codelst:1.0.0": sourcefile
#let hlfile(filename) = sourcefile(read(filename), file: filename)
#let apply(body) = {
//  let body-font-settings = (font: "Nunito Sans 10pt", size: 12pt, stretch: 75%)
//  let body-font-settings = (font: "Hanken Grotesk", size: 12pt, stretch: 75%)
//  let body-font-settings = (font: "Lato", size: 11pt)
  let body-font-settings = (font: "New Computer Modern", size: 12pt, weight: 450)
  let page-margin = (left: 0.75in, right: 0.25in, top: 0.35in, bottom: 0.15in)
  let margin = (left: 0.75in, right: 0.25in, top: 2em, bottom: 2em)
  let page-frame-thickness = 1.5pt
  set page(
    margin: (..page-margin, bottom: margin.bottom + 2em),
    numbering: "1",
    background: align(top + start, pad(..margin, rect(width: 100%, height: 100%, stroke: page-frame-thickness + gray))),
    footer: locate(loc => align(center, move(dy: -margin.bottom + 1em, text(..body-font-settings, size: 9pt, counter(page).display(loc.page-numbering())))))
  )
  show: block.with(breakable: true, width: 100%, inset: page-frame-thickness + 1em)
  
  set text(..body-font-settings)

  let code-color = rgb("#f4f4f4")
//  show raw: set text(font: "CommitMono", size: 1.1em)
//  show raw: set text(font: "Inconsolata", size: 1.1em)
//  show raw: set text(font: "Source Code Pro", size: 1.1em)
//  show raw: set text(font: "Iosevka Fixed", size: 1.1em)
  show raw: set text(font: "New Computer Modern Mono", size: 1.2em)
  show raw.where(block: false): box.with(
    fill: code-color,
    inset: (x: 4pt, y: 0pt),
    outset: (y: 4pt),
    radius: 2pt,
  )
  show raw.where(block: true): block.with(
    fill: code-color,
    inset: 10pt,
    radius: 4pt,
    width: 100%,
  )
  show raw.where(block: true): it => align(left, it)
  set raw(theme: "vendor/gr.tmTheme")
  set par(leading: 0.5em)
  body
}

#let assignment(number) = {
  set align(center)
  [== Assignment #number]
}
#let objective(body) = align(center, [*Objective*: #body])
#let oset(kind) = block(spacing: 0.6em, [===== #h(1em) #kind])

