```
Enter the starting day (1 for Sunday, 2 for Monday, ..., 7 for Saturday): 1
Enter the number of days in the month: 30
Calender for this month:
 SUN MON TUE WED THU FRI SAT
   1   2   3   4   5   6   7
   8   9  10  11  12  13  14
  15  16  17  18  19  20  21
  22  23  24  25  26  27  28
  29  30
```
```
Enter the starting day (1 for Sunday, 2 for Monday, ..., 7 for Saturday): 2
Enter the number of days in the month: 28
Calender for this month:
 SUN MON TUE WED THU FRI SAT
       1   2   3   4   5   6
   7   8   9  10  11  12  13
  14  15  16  17  18  19  20
  21  22  23  24  25  26  27
  28
```
```
Enter the starting day (1 for Sunday, 2 for Monday, ..., 7 for Saturday): 7
Enter the number of days in the month: 31
Calender for this month:
 SUN MON TUE WED THU FRI SAT
                           1
   2   3   4   5   6   7   8
   9  10  11  12  13  14  15
  16  17  18  19  20  21  22
  23  24  25  26  27  28  29
  30  31
```
