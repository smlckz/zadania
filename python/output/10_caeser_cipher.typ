```
Enter message: A quick brown fox jumps over a lazy dog.
Enter key (1-25, -1 - -25): 5
Ceesar cipher with k = 5 applied to the given message:
F vznhp gwtbs ktc ozrux tajw f qfed itl.
```
```
Enter message: F vznhp gwtbs ktc ozrux tajw f qfed itl.
Enter key (1-25, -1 - -25): -5
Ceesar cipher with k = 21 applied to the given message:
A quick brown fox jumps over a lazy dog.
```
