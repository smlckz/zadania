>>> import math
>>> math.cos(math.pi / 3)
0.5000000000000001
>>> math.sin(math.pi / 6)
0.49999999999999994
>>> math.tan(math.pi / 4)
0.9999999999999999
>>> math.pow(1.123, 0.123)
1.0143707323622344
>>> math.exp(379)
3.959210944514706e+164
>>> math.log(10, 2)
3.3219280948873626
>>> math.hypot(3, 4)
5.0
>>> math.hypot(3, 4, 5)
7.0710678118654755
>>> math.degrees(math.pi / 4)
45.0
>>> math.radians(90) / math.pi
0.5
>>> [*(print(i, math.sqrt(i)) for i in range(1, 10))]
1 1.0
2 1.4142135623730951
3 1.7320508075688772
4 2.0
5 2.23606797749979
6 2.449489742783178
7 2.6457513110645907
8 2.8284271247461903
9 3.0
[None, None, None, None, None, None, None, None, None]>>> math.modf(12.5)
(0.5, 12.0)
>>> math.gamma(6)
120.0
>>> [math.floor(x) for x in [12.3, -12.3]]
[12, -13]
>>> [math.ceil(x) for x in [12.3, -12.3]]
[13, -12]
>>> [math.trunc(x) for x in [12.3, -12.3]]
[12, -12]
>>> math.cbrt(8), math.cbrt(10)
(2.0, 2.154434690031884)
>>> math.dist((1, 1), (2, 3))
2.23606797749979
>>> math.dist((1, 1), (5, 4))
5.0
>>> math.isqrt(23)
4
>>> math.exp2(10)
1024.0

