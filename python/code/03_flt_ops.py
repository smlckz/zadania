a = float(input('Enter first number: '))
b = float(input('Enter second number: '))
print('Concatenation of integer parts:', ''.join(str(int(i)) for i in (a, b)))
print('Sum rounded to 2 decimal places:', round(a + b, 2))
