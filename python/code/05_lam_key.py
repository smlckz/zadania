items = [(0, 'd'), (1, 'a'), (2, 'c'), (3, 'b')]

print(max(items, key=lambda it: it[0]))
print(min(items, key=lambda it: it[1]))
items.sort(key=lambda x: ord(x[1]) - x[0])
print(items)
