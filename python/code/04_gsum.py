def gaussian_sum_even(numbers):
    assert len(numbers) % 2 == 0
    start_index, end_index = 0, len(numbers) - 1
    sum = 0
    while start_index < end_index:
        print(numbers[start_index], '+', numbers[end_index])
        sum += numbers[start_index] + numbers[end_index]
        start_index += 1
        end_index -= 1
    print('Result:', sum)

gaussian_sum_even(list(range(1, 7)))

def gaussian_sum(numbers):
    start_index, end_index = 0, len(numbers) - 1
    sum = 0
    while start_index < end_index:
        print(numbers[start_index], '+', numbers[end_index])
        sum += numbers[start_index] + numbers[end_index]
        start_index += 1
        end_index -= 1
    middle_value = numbers[len(numbers) // 2]
    print(middle_value)
    sum += middle_value
    print('Result:', sum)

gaussian_sum(list(range(1, 10)))
