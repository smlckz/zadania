size = int(input('Enter pattern size: '))
if size < 0:
    print('Invalid size')
else:
    print('*' * size)
    for i in range(size - 2):
        print('*' + ' ' * (size - 2) + '*')
    if size > 1:
        print('*' * size)
