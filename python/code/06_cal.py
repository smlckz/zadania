import math, sys
start_day = int(input('Enter the starting day (1 for Sunday, 2 for Monday, ..., 7 for Saturday): '))
if not (1 <= start_day <= 7):
    print('Invalid day, must be between 1 and 7, inclusive')
    sys.exit(1)
days_in_month = int(input('Enter the number of days in the month: '))
print('Calender for this month:')
print(' SUN MON TUE WED THU FRI SAT')
if start_day == 2:
    print(' ' * 4, end='')
else:
    print(' ' * (4 * (start_day - 1)), end='')

day = 1
if start_day != 1:
    for i in range(7 - start_day + 1):
        print(f' {day:3}', end='')
        day += 1
    print()
    weeks = math.ceil((days_in_month - (7 - start_day + 1)) / 7)
else:
    weeks = math.ceil(days_in_month / 7)

class StopIt(Exception): pass

try:
    for w in range(weeks):
        for i in range(7):
            if day > days_in_month:
                raise StopIt()
            print(f' {day:3}', end='')
            day += 1
        print()
except StopIt: pass
print()
