#import "@preview/codelst:1.0.0": sourcefile, sourcecode
#import "@preview/tablex:0.0.7": tablex, cellx

#let apply(body, page-numbering: none) = {
  let body-font-settings = (font: "Nunito Sans 10pt", size: 12pt, stretch: 75%)
  let page-margin = (left: 0.75in, right: 0.25in, top: 0.35in, bottom: 0.15in)
  let margin = (left: 0.75in, right: 0.25in, top: 2em, bottom: 2em)
  let page-frame-thickness = 1.5pt
  set page(
    margin: (..page-margin, bottom: margin.bottom + 2em),
    numbering: if page-numbering != none { page-numbering } else { "1" },
    background: align(top + start, pad(..margin, rect(width: 100%, height: 100%, stroke: page-frame-thickness + gray))),
    footer: locate(loc => align(center, move(dy: -margin.bottom + 1em, text(..body-font-settings, size: 9pt, counter(page).display(loc.page-numbering())))))
  )
  show: block.with(breakable: true, width: 100%, inset: page-frame-thickness + 1em)
  
  set text(..body-font-settings)

  let code-color = rgb("#f4f4f4")
  show raw: set text(font: "Iosevka Fixed", size: 1.1em)
  show raw.where(block: false): box.with(
    fill: code-color,
    inset: (x: 4pt, y: 0pt),
    outset: (y: 4pt),
    radius: 2pt,
  )
  show raw.where(block: true): block.with(
    fill: code-color,
    inset: 10pt,
    radius: 4pt,
    width: 100%,
  )
  show raw.where(block: true): it => align(left, it)
  set raw(theme: "vendor/gr.tmTheme")
  set par(leading: 0.5em)
  body
}

#let cover(page-count) = apply(page-numbering: (..nums) => {}, [
  #set align(center)
  #v(1in)
  #set text(font: "Hanken Grotesk")
  #set text(size: 40pt)
  University of ---
  #v(0.5in)
  #set text(size: 30pt)
  B.Sc. Honours Semester V --- \
  Examination 20---
  #v(0.35in)
  #set text(font: "Inter", size: 25pt)
  Practical Assignments \ _on_ \ Python 3 Programming Lab
  #v(1in)
  #set text(font: "Hanken Grotesk", size: 20pt)
  #table(
    columns: 2,
    stroke: none,
    align: left,
    gutter: 5pt,
    [*Roll No.:*], [---],
    [*Registration No.:*], [---],
    [*Subject Code:*], [---],
    [*Paper Code:*], [---],
    [*Number of pages:*], [#page-count]
  )
])

#locate(loc => { 
  let last-page-number = counter(page).final(loc).first()
  cover(last-page-number)
  colbreak()
})

#let highlight-code-file(filename) = sourcefile(read(filename), file: filename)

#let highlight-output(body) = sourcecode(numbering: none, body)

#let signature() = {
  align(right, block(width: 15em)[
    #v(5em)
    #line(length: 100%) \
    #v(-2.5em)
    #align(center)[Teacher’s signature]
  ])
}

#let alist = state("assignment-list", ())

#let list-of-assignments(contents) = locate(loc => {
  let assignment-list = alist.final(loc)
  let last-page-number = counter(page).final(loc).first()
  contents(assignment-list, last-page-number)
})

#counter(page).update(1)

#apply(page-numbering: "(i)" , [
  #let heading-format(content) = cellx(align: center + horizon, content)
  #let column-alignments = (right, auto, center + horizon, center + horizon, auto)
  #let preprocess-alist(assignment-list, last-page-number) = {
    let index = 0
    let last-index = assignment-list.len() - 1
    let page-number-list = ()
    while index < last-index {
      let item = assignment-list.at(index)
      let next-item = assignment-list.at(index + 1)
      let starting-page-number = item.page-number
      let finishing-page-number = next-item.page-number - if next-item.new { 1 } else { 0 }
      page-number-list.push((starting-page-number, finishing-page-number))
      index = index + 1
    }
    page-number-list.push((assignment-list.at(last-index).page-number, last-page-number))
    let new-assignment-list = ()
    index = 0
    for (start, end) in page-number-list {
      let page-number = if start >= end [#start] else [#start - #end]
      let assignment = assignment-list.at(index)
      let serial-number = [#{assignment.number}. ]
      let description = stack(dir: ltr, v(5em), assignment.description)
      let item = (serial-number, description, page-number, assignment.date, [])
      new-assignment-list.push(item)
      index = index + 1
    }
    new-assignment-list
  }
  #list-of-assignments((assignment-list, last-page-number) => {
    counter(page).update(1)
    align(center, [== Contents])
    tablex(
      columns: (3em, 1fr, 4em, 6em, 11em),
      stroke: 1pt + gray,
      repeat-header: true,
      map-cols: (i, cells) => (cells.first(), ..cells.slice(1).map(cell => (..cell, align: column-alignments.at(i)))),
      heading-format[*Sl.* \ *No.*], heading-format[*Description*], heading-format[*Page No.*], heading-format[*Date*], heading-format[*Teacher’s* \ *Signature*],
      ..preprocess-alist(assignment-list, last-page-number).flatten(),
    )
    v(20em)
    signature()
  })
])

#colbreak()

#let list-of-dates = ([11/09/2023], [16/09/2023], [26/09/2023], [04/10/2023], [09/10/2023], [10/10/2023], [13/10/2023], [16/10/2023], [21/11/2023], [28/11/2023], [02/12/2023], [04/12/2023], [04/12/2023], [05/12/2023], [19/12/2023], [02/01/2024], [08/01/2024], [09/01/2024], [15/01/2024], [18/01/2024])
#let assignment-dates-indices = (1, 2, 3, 4, 5, 5, 6, 6, 7, 7, 8, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 15, 16, 17, 18, 19)

#let assignment(number, description, next: false, new: false) = {
  let date = list-of-dates.at(assignment-dates-indices.at(number - 1) - 1)
  [=== #text(weight: 600, [Assignment #number]): #text(weight: 400, {
    description
    if next == false {
      h(1fr)
      [Date: #date]
    }
  })]
  if next == true {
    align(right, [Date: #date])
  }
  locate(loc => alist.update(lst => (..lst, (number: number, description: description, page-number: counter(page).at(loc).first(), date: date, new: new))))
}

#show: A => apply(A)
#counter(page).update(1)
#assignment(1)[Using the Python _interactive interpreter_, demonstrate at least 15 functions in the math module.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/01_math_functions.py")
#signature()
#v(5em)
#assignment(2)[Write a Python program to take two numbers from the user and show their the sum, product, difference and the GCD.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/02_num_ops.py")
==== Output
#highlight-output[```
Enter first number: 12
Enter second number: 8
Sum: 20
Product: 96
Difference: 4
GCD: 4
```]
#signature()
#colbreak()
#assignment(3, new: true)[Write a program to take two floating point numbers as input from the user. Concatenate the integral parts of the two numbers and display them, also display the sum of the input floating point numbers rounded upto 2 decimal places.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/03_flt_ops.py")
#highlight-output[```
Enter first number: 12.345
Enter second number: 678.9999
Concatenation of integer parts: 12678
Sum rounded to 2 decimal places: 691.34
```]
#signature()
#v(3em)
#assignment(4)[Write a Python program to calculate the sum a list of number of (i) even length and then (ii) any length, using `while` loop: at each step, add $k$th number from the start and end of the list and display it, for $k$ from $0$ to half the length the list.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/04_gsum.py")
==== Output
#highlight-output[```
1 + 6
2 + 5
3 + 4
Result: 21
1 + 9
2 + 8
3 + 7
4 + 6
5
Result: 45
```]
#signature()
#v(7em)
#assignment(5)[Write a Python program to demonstrate keyword argument `key` of `sum()`, `min()`, `max()`, and `sort()` functions using `lambda`s.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/05_lam_key.py")
==== Output
#highlight-output[```
(3, 'b')
(1, 'a')
[(3, 'b'), (1, 'a'), (2, 'c'), (0, 'd')]
```]
#signature()
#v(3em)
#assignment(6)[Write a Python program to generate the calendar of a month given the start day (`1` for Sunday, `2` for Monday, ... `7` is Saturday) and the number of days in the month.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/06_cal.py")
==== Output
#highlight-output[```
Enter the starting day (1 for Sunday, 2 for Monday, ..., 7 for Saturday): 2
Enter the number of days in the month: 28
Calender for this month:
 SUN MON TUE WED THU FRI SAT
       1   2   3   4   5   6
   7   8   9  10  11  12  13
  14  15  16  17  18  19  20
  21  22  23  24  25  26  27
  28
```]
#signature()
#v(3em)
#assignment(7, next: true)[#table(
    columns: (1fr, 4em), stroke: none,
    inset: 0pt, gutter: 0pt,
    column-gutter: 5pt,
    [Write a Python program to print the following star pattern of the given size.],
    [
      #set par(leading: 0em)
      #set block(inset: 0pt, width: 4em, outset: 0pt)
      ```text
****
*  *
*  *
****
```
  ])]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/07_hstarpat.py")
==== Output
#highlight-output[```
Enter pattern size: 6
******
*    *
*    *
*    *
*    *
******
```]
#signature()
#v(3em)
#assignment(8)[Write a Python program to from a list of words, join all the words in the odd and even indices to form two strings using list slicing and `join` method.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/08_odd_even_join.py")
==== Output
#highlight-output[```
Words at odd indices: abc ghi mno tuv
Words at even indices: def jkl pqrs wxyz
```]
#signature()
#v(2em)
#assignment(9, next: true, new: true)[Write a Python program to implement a function that works like `str.count()` method.]
#set par(leading: 0.6em)
==== Source Code
#highlight-code-file("code/09_str_count.py")
==== Output
#highlight-output[```
Enter a string: Mississippi
Enter substring to count: iss
Enter starting and ending indices (default: whole string):
Count: 2
```]
#highlight-output[```
Enter a string: Mississippi
Enter substring to count: issi
Enter starting and ending indices (default: whole string):
Count: 1
```]
#signature()
#v(2em)
#assignment(10, new: true)[Write a Python program that take a plain text string and generate cyphertext by using $k$th next character with wrap around for each character for a given constant key value $k$. (Also known as _Caesar Cipher_.)]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/10_caeser_cipher.py")
==== Output
#highlight-output[```
Enter message: A quick brown fox jumps over a lazy dog.
Enter key (1-25, -1 - -25): 5
Ceesar cipher with k = 5 applied to the given message:
F vznhp gwtbs ktc ozrux tajw f qfed itl.
```]
#signature()
#v(4em)
#assignment(11, new: true)[Write a Python program to simulate a stack and a queue using lists using functions.]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/11_sqem.py")
==== Output
#highlight-output[```
Stack: [1, 2, 3]
Popped from stack: 3
Stack after pop: [1, 2]

Queue: [1, 2, 3]
Dequeued from queue: 1
Queue after dequeue: [2, 3]
```]
#signature()
#v(3em)
#assignment(12, new: true)[Write a Python program to find the third largest number in a list of numbers without sorting.]
#set par(leading: 0.6em)
==== Source Code
#highlight-code-file("code/12_3rd_max.py")
==== Output
#highlight-output[```
List: [12, 5, 23, 8, 18, 2, 32]
Third largest number: 18
```]
#signature()
#colbreak()
#assignment(13, next: true, new: true)[Write a Python program to use the `map()` method to square each number in a list.]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/13_map_sqr.py")
==== Output
#highlight-output[```
Original Numbers: [1, 2, 3, 4, 5]
Squared Numbers: [1, 4, 9, 16, 25]
```]
#signature()
#v(5em)
#assignment(14, next: true)[Write a Python program that, given sequences of first and last names, generate another sequence which contains the full name of each person, using the `zip()` function.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/14_zip_names.py")
==== Output
#highlight-output[```
Generated Full Names:
Li Wei
Chen Yuan
Wang Mei
Sato Haruto
Tanaka Aoi
Ito Sakura
Kim Min-Jae
Lee Eun-Ji
Park Ji-Hoon
```]
#signature()
#v(2em)
#assignment(15, next: true)[Write a Python program to take a list of numbers (both positive and negative) and make a new tuple out from this list with only the positive values using generator expression.]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/15_filter_pos_gen.py")
==== Output
#highlight-output[```
Original Numbers: [5, -3, 8, -2, 10, -7, 15, -1]
Positive Numbers Tuple: (5, 8, 10, 15)
```]
#signature()
#colbreak()
#assignment(16, new: true)[Write a Python program to implement the Jaccard and cosine similarity of two sets using the set union and intersection operations.]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/16_set_simil.py")
==== Output
#highlight-output[```
Set A: {1, 2, 3, 4, 5}
Set B: {3, 4, 5, 6, 7}
Jaccard Similarity: 0.42857142857142855
Cosine Similarity: 0.12
```]
#signature()
#colbreak()
#assignment(17, new: true)[Write a Python program that generates a set of prime numbers and another set of odd numbers to demonstrate the result of _union_, _intersection_, _difference_, _symmetric difference_ operations on these sets.]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/17_set_po.py")
==== Output
#highlight-output[```
Prime Numbers: {2, 3, 5, 37, 7, 41, 11, 43, 13, 47, 17, 19, 23, 29, 31}
Odd Numbers: {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49}

Union: {1, 2, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49}
Intersection: {3, 37, 5, 7, 41, 11, 43, 13, 47, 17, 19, 23, 29, 31}
Difference (Prime - Odd): {2}
Symmetric Difference: {1, 2, 9, 15, 21, 25, 27, 33, 35, 39, 45, 49}
```]
#signature()
#colbreak()
#assignment(18, new: true)[Write a Python function which takes a string and returns a dictionary containing the number of words and letters in the provided string.]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/18_count_wl.py")
==== Output
#highlight-output[```
Input String: Hello, this is a sample string with 12 words and 43 letters.
Result: {'num_words': 12, 'num_letters': 43}
```]
#signature()
#v(2em)
#assignment(19, next: true)[
  Write a Python program with a function to generate the Fibonacci numbers in
  #set enum(numbering: "(a)")
  + Exponential time using the naïve algorithm.
  + Linear time using dynamic programming (memoization) with a dictionary.
]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/19_fib_2way.py")
==== Output
#highlight-output[```
Fibonacci Sequence (Exponential Time): [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181]
Fibonacci Sequence (Linear Time with Memoization): [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181]
```]
#signature()
#v(2em)
#assignment(20)[Write a Python program to store a sparse matrix as a dictionary.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/20_spmx.py")
==== Output
#highlight-output[```
Sparse Matrix:
0 5 0 0
0 0 7 0
0 0 0 3
```]
#signature()
#v(3em)
#assignment(21)[Write a Python program to implement quick sort and merge sort algorithms to sort lists of numbers.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/21_qm_sort.py")
==== Output
#highlight-output[```
Original List: [38, 27, 43, 3, 9, 82, 10]
Quick Sort: [3, 9, 10, 27, 38, 43, 82]
Merge Sort: [3, 9, 10, 27, 38, 43, 82]
```]
#signature()
#v(5em)
#assignment(22, next: true)[Write a Python program that displays the Pascal’s triangle of a given size.]
#set par(leading: 0.7em)
==== Source Code
#highlight-code-file("code/22_pascal.py")
#v(4em)
==== Output
#highlight-output[```
Pascal's Triangle (Size 10):
              1
             1 1
            1 2 1
           1 3 3 1
          1 4 6 4 1
        1 5 10 10 5 1
       1 6 15 20 15 6 1
     1 7 21 35 35 21 7 1
    1 8 28 56 70 56 28 8 1
 1 9 36 84 126 126 84 36 9 1
```]
#signature()
#v(2em)
#assignment(23)[Write a Python program with a function to generate all Pythagorean triples in a certain range.]
#set par(leading: 0.5em)
*Definition*: Three positive integers $a$, $b$, and $c$ are _Pythagorean triples_ if $a^2 + b^2 = c^2$.
==== Source Code
#highlight-code-file("code/23_ptprl.py")
==== Output
#highlight-output[```
Pythagorean Triples in the range 1 to 20:
(3, 4, 5)
(5, 12, 13)
(6, 8, 10)
(8, 15, 17)
(9, 12, 15)
(12, 16, 20)
```]
#signature()
#v(3em)
#assignment(24)[Write a Python program to open a file and count the number of letters, words and special characters.]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/24_file_lws.py")
==== File ```text sample_text.txt```
#sourcefile(numbering: none, lang: "text", read("code/sample_text.txt"))
==== Output
#highlight-output[```
Number of letters: 369
Number of words: 69
Number of special characters: 10
```]
#signature()
#v(3em)
#assignment(25)[Write a Python program to create a file, write some content in it, then copy the contents of this file into another one.]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/25_file_copy.py")
==== Output
#highlight-output[```
Content copied successfully!
```]
==== File ```text input_file.txt```
#sourcefile(numbering: none, lang: "text", read("code/input_file.txt"))
==== File ```text output_file.txt```
#sourcefile(numbering: none, lang: "text", read("code/output_file.txt"))
#signature()
#v(4em)
#assignment(26)[
  #set enum(numbering: (..nums) => [--])
Create a `Graph` class to store and manipulate graphs. It should have the following functions:
  + Read an edge list file, where each edge `(u, v)` appears exactly once in the file as space separated values.
  + Add and remove nodes and edges.
  + Print nodes, and edges in a user readable format.
  + Computes basic statistics of the graph like degree distribution, clustering coefficient, and the number of connected components.
  + Finding all the neighbors of a node.
  + Finding all the connected components and storing them as individual Graph objects inside the class.
  + Finding single source shortest paths using Breadth First Search.
]
#set par(leading: 0.6em)
==== Source Code
#highlight-code-file("code/26_graph.py")
==== Output
#highlight-output[```
Nodes and Edges:
Nodes: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
Edges:(1, 2) (1, 3) (1, 4) (2, 1) (2, 3) (2, 4) (2, 5) (3, 1) (3, 2) (3, 4) (3, 5) (3, 6) (4, 1) (4, 2) (4, 3) (4, 5) (4, 6) (4, 7) (5, 2) (5, 3) (5, 4) (5, 6) (5, 7) (5, 8) (6, 3) (6, 4) (6, 5) (6, 7) (6, 8) (6, 9) (7, 4) (7, 5) (7, 6) (7, 8) (7, 9) (7, 10) (8, 5) (8, 6) (8, 7) (8, 9) (8, 10) (8, 11) (9, 6) (9, 7) (9, 8) (9, 10) (9, 11) (9, 12) (10, 7) (10, 8) (10, 9) (10, 11) (10, 12) (10, 13) (11, 8) (11, 9) (11, 10) (11, 12) (11, 13) (11, 14) (12, 9) (12, 10) (12, 11) (12, 13) (12, 14) (12, 15) (13, 10) (13, 11) (13, 12) (13, 14) (13, 15) (13, 16) (14, 11) (14, 12) (14, 13) (14, 15) (14, 16) (14, 17) (15, 12) (15, 13) (15, 14) (15, 16) (15, 17) (15, 18) (16, 13) (16, 14) (16, 15) (16, 17) (16, 18) (16, 19) (17, 14) (17, 15) (17, 16) (17, 18) (17, 19) (17, 20) (18, 15) (18, 16) (18, 17) (18, 19) (18, 20) (19, 16) (19, 17) (19, 18) (19, 20) (20, 17) (20, 18) (20, 19) (21, 22) (21, 23) (22, 21) (22, 23) (22, 24) (23, 21) (23, 22) (23, 24) (24, 22) (24, 23)

Degree Distribution:
{2: 2, 3: 4, 4: 2, 5: 2, 6: 14}

Clustering Coefficient of Node 7: 1.2

Connected Components:
Component 1: Nodes - [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
Component 2: Nodes - [21, 22, 23, 24]

Single Source Shortest Paths from Node 1:
{1: 0, 2: 1, 3: 1, 4: 1, 5: 2, 6: 2, 7: 2, 8: 3, 9: 3, 10: 3, 11: 4, 12: 4, 13: 4, 14: 5, 15: 5, 16: 5, 17: 6, 18: 6, 19: 6, 20: 7, 21: inf, 22: inf, 23: inf, 24: inf}
```]
#v(30em)
#signature()


#{/*
#assignment()[]
#set par(leading: 0.5em)
==== Source Code
#highlight-code-file("code/")
==== Output
#highlight-output[```
```]
#signature()
  */[]}

