#import "tpl.typ": *
#show: body => apply(body)
#show heading: set text(font: "New Computer Modern Sans", weight: 450)
#let semibold(body) = text(weight: 600, body)
#let roman(body) = text(weight: 400, body)
// #text(size: 1.3em, font: "New Computer Modern Sans", align(center)[#semibold[Practical Assignments] \ _on_ \ #semibold[Programming in Python Lab]])
#text(size: 1.3em, font: "New Computer Modern Sans", align(center)[#semibold[Programming in Python Lab]])
#align(center, text(size: 1.1em)[DSE-B-1 \ Use of user defined functions are encouraged wherever applicable])
#set enum(full: true, numbering: (..args) => {
  let patterns = ("1.", "(a)")
  let pattern = patterns.at(calc.min(args.pos().len(), patterns.len()) - 1)
  numbering(pattern, args.pos().last())
})
#let nntiv = body => [
  #set text(size: 0.9em, fill: rgb("#777777"))
  \ #body
]

#set raw(lang: "python")
#set par(justify: true)
#set heading(numbering: "1 ")

+ Using the Python _interactive interpreter_, demonstrate at least 15 functions in the math module.
  #nntiv[For example, to find the GCD of two numbers, area and perimeter of circle using `math.pi` etc.]
+ Write a Python program to take two numbers from the user and show their the sum, product, difference and the GCD.
+ Write a program to take two floating point numbers as input from the user. Concatenate the integral parts of the two numbers and display them, also display the sum of the input floating point numbers rounded upto 2 decimal places.
+ Write a Python program to calculate the sum a list of number of (i) even length and then (ii) any length, using `while` loop: at each step, add $k$th number from the start and end of the list and display it, for $k$ from $0$ to half the length the list.
  #nntiv[For example: if the list is `[1, 2, 3, 4, 5, 6]`, the program should output `1 + 6`, `2 + 5`, and `3 + 4` in separate lines, and the result of the addition `21`.]
+ Write a Python program to demonstrate keyword argument `key` of `sum()`, `min()`, `max()`, and `sort()` functions using `lambda`s.
+ Write a Python program to generate the calendar of a month given the start day (`1` for Sunday, `2` for Monday, ... `7` is Saturday) and the number of days in the month.
  #nntiv[An example:]
  #[
    #show raw: block.with(inset: 0pt, fill: white, width: 20em, outset: 0pt)
    #show: it => align(center, it)
    ```text
    Enter the starting day: 5
    Enter the number of days in the month: 30
    Calender for this month:
    SUN MON TUE WED THU FRI SAT
                      1   2   3
      4   5   6   7   8   9  10
     11  12  13  14  15  16  17
     18  19  20  21  22  23  24
     25  26  27  28  29  30
     ```]
+ #table(
    columns: (1fr, 4em), stroke: none,
    inset: 0pt, gutter: 0pt,
    column-gutter: 5pt,
    [Write a Python program to print the following star pattern of the given size. \
    #nntiv[Example (with size 4):]],
    [
      #set par(leading: 0em)
      #set block(inset: 0pt, width: 4em, outset: 0pt)
      ```text
****
*  *
*  *
****
```
  ])
+ Write a Python program to from a list of words, join all the words in the odd and even indices to form two strings using list slicing and `join` method.
  #nntiv[Example: for a list `['abc', 'def', 'ghi', 'jkl', 'mno', 'pqrs', 'tuv', 'wxyz']`, the result are: `'abcghimnotuv'` and `'defjklpqrswxyz'`.]
+ Write a Python program to implement a function that works like `str.count()` method.
+ Write a Python program that take a plain text string and generate cyphertext by using $k$th next character with wrap around for each character for a given constant key value $k$.
  #nntiv[Also known as _Caesar Cipher_. \ Example: for key $k = 23$, the message `"The quick brown fox jumps over the lazy dog."` encrypts to `"Qeb nrfzh yoltk clu grjmp lsbo qeb ixwv ald."`.]
+ Write a Python program to simulate a stack and a queue using lists using functions.
+ Write a Python program to find the third largest number in a list of numbers without sorting.
+ Write a Python program to use the `map()` method to square each number in a list.
+ Write a Python program that, given sequences of first and last names, generate another sequence which contains the full name of each person, using the `zip()` function.
+ Write a Python program to take a list of numbers (both positive and negative) and make a new tuple out from this list with only the positive values using generator expression.
+ Write a Python program to implement the Jaccard and cosine similarity of two sets using the set union and intersection operations.
+ Write a Python program that generates a set of prime numbers and another set of odd numbers to demonstrate the result of _union_, _intersection_, _difference_, _symmetric difference_ operations on these sets.
+ Write a Python function which takes a string and returns a dictionary containing the number of words and letters in the provided string.
+ Write a Python program with a function to generate the Fibonacci numbers in
  + Exponential time using the naïve algorithm.
  + Linear time using dynamic programming (memoization) with a dictionary.
+ Write a Python program to store a sparse matrix as a dictionary.
+ Write a Python program to implement quick sort and merge sort algorithms to sort lists of numbers.
+ Write a Python program that displays the Pascal’s triangle of a given size.
+ Three positive integers $a$, $b$, and $c$ are Pythagorean triples if $a^2 + b^2 = c^2$. Write a Python program with a function to generate all Pythagorean triples in a certain range.
+ Write a Python program to open a file and count the number of letters, words and special characters.
+ Write a Python program to create a file, write some content in it, then copy the contents of this file into another one.
+ Create a `Graph` class to store and manipulate graphs. It should have the following functions:
  + Read an edge list file, where each edge `(u, v)` appears exactly once in the file as space separated values.
  + Add and remove nodes and edges.
  + Print nodes, and edges in a user readable format.
  + Computes basic statistics of the graph like degree distribution, clustering coefficient, and the number of connected components.
  + Finding all the neighbors of a node.
  + Finding all the connected components and storing them as individual Graph objects inside the class.
  + Finding single source shortest paths using Breadth First Search.

