zadania
=======

大学第五学期的编程作业。

Papers
------

- Core courses

  + Database Management System

    CC-11: RDBMS lab using MySQL and PHP.
  + Object-oriented programming

    CC-12: OOP lab Using Java.

- Discipline-specific electives

  + Digital Image Processing

    DSE-A-1: DIP lab based on OpenCV in Python.
  + Programming with Python

    DSE-B-2: Python 3 Programming Lab

