#import "@preview/tablex:0.0.7": tablex, cellx
#import "/template.typ": apply

#let cover(page-count) = apply(page-numbering: (..nums) => {}, [
  #set align(center)
  #v(1in)
  #set text(font: "Hanken Grotesk")
  #set text(size: 40pt)
  University of ---
  #v(0.5in)
  #set text(size: 30pt)
  B.Sc. Honours Semester V --- \
  Examination 20---
  #v(0.35in)
  #set text(font: "Inter", size: 25pt)
  Practical Assignments \ _on_ \ Digital Image Processing Lab \ using OpenCV and Python
  #v(1in)
  #set text(font: "Hanken Grotesk", size: 20pt)
  #tablex(
    columns: 2,
    stroke: none,
    align: left,
    gutter: 5pt,
    [*Roll No.:*], [---],
    [*Registration No.:*], [---],
    [*Subject Code:*], [---],
    [*Paper Code:*], [---],
    [*Number of pages:*], page-count
  )
])

#locate(loc => { 
  let last-page-number = counter(page).final(loc).first()
  cover(last-page-number)
  colbreak()
})

