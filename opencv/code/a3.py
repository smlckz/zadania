import cv2
import matplotlib.pyplot as plt

image = cv2.imread("1i.png", cv2.IMREAD_GRAYSCALE)

histogram = cv2.calcHist([image], [0], None, [256], [0, 256])

plt.figure(figsize=(8, 5))
plt.plot(histogram, color="black")
plt.title("Grayscale Histogram")
plt.xlabel("Pixel Value")
plt.ylabel("Frequency")
plt.xlim([0, 256])
plt.savefig("3.svg")
