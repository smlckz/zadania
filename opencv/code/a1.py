import cv2
import numpy as np
import os

os.chdir("images")

image = cv2.imread("1i.png")

gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

_, graytobin = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY)

_, rgbtobin = cv2.threshold(image, 150, 255, cv2.THRESH_BINARY)

hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)

hsvtorgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)

rcb = cv2.cvtColor(image, cv2.COLOR_BGR2YCR_CB)

rcbtorgb = cv2.cvtColor(rcb, cv2.COLOR_YCrCb2BGR)

cv2.imwrite("1.1.png", gray)
cv2.imwrite("1.2.png", hsv)
cv2.imwrite("1.3.png", rcb)
cv2.imwrite("1.4.png", hsvtorgb)
cv2.imwrite("1.5.png", rcbtorgb)
cv2.imwrite("1.6.png", graytobin)
cv2.imwrite("1.7.png", rgbtobin)
