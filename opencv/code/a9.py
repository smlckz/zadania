import cv2

image1 = cv2.imread("9i1.jpg", cv2.IMREAD_GRAYSCALE)
image2 = cv2.imread("9i2.jpg", cv2.IMREAD_GRAYSCALE)

assert image1.shape == image2.shape, "Input images must have the same dimensions"

result_image = cv2.subtract(image1, image2)

cv2.imwrite("9o.jpg", 255 - result_image)
