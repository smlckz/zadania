import cv2
import numpy as np
import matplotlib.pyplot as plt

images = [cv2.imread(path, cv2.IMREAD_GRAYSCALE) for path in ["9i1.jpg", "9i2.jpg"]]
assert all(
    image.shape == images[0].shape for image in images
), "Input images must have the same dimensions"

operations = [
    ("Addition", lambda x, y: cv2.add(x, y)),
    ("Subtraction", lambda x, y: cv2.subtract(x, y)),
    ("Multiplication", lambda x, y: cv2.multiply(x, y)),
    (
        "Division",
        lambda x, y: cv2.divide(
            x.astype(np.float32), np.where(y == 0, 1, y).astype(np.float32)
        ),
    ),
]

plt.figure(figsize=(8, 6))

for i, image in enumerate(images, start=1):
    plt.subplot(3, 2, i)
    plt.imshow(image, cmap="gray")
    plt.title(f"Image {i}")
    plt.axis("off")

for i, (operation_name, operation_function) in enumerate(
    operations, start=len(images) + 1
):
    result_image = operation_function(*images)
    plt.subplot(3, 2, i)
    plt.imshow(result_image, cmap="gray")
    plt.title(operation_name)
    plt.axis("off")

plt.savefig("10.svg")
