import cv2
import numpy as np
import matplotlib.pyplot as plt

input_image = cv2.imread("4i.jpg", cv2.IMREAD_GRAYSCALE)

kernel_size = 3

filters = [
    (input_image, "Original Image"),
    (cv2.blur(input_image, (kernel_size, kernel_size)), "Mean Filtered"),
    (cv2.GaussianBlur(input_image, (kernel_size, kernel_size), 0), "Weighted Average Filtered"),
    (cv2.medianBlur(input_image, kernel_size), "Median Filtered"),
    (cv2.dilate(input_image, np.ones((kernel_size, kernel_size), np.uint8)), "Max Filtered"),
    (cv2.erode(input_image, np.ones((kernel_size, kernel_size), np.uint8)), "Min Filtered"),
]

plt.figure(figsize=(12, 10))

for i, (filtered_image, title) in enumerate(filters, start=1):
    plt.subplot(3, 3, i)
    plt.imshow(filtered_image, cmap="gray")
    plt.title(title)
    plt.axis("off")

plt.savefig("12.svg")
