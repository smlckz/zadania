import cv2

image1 = cv2.imread("9i1.jpg", cv2.IMREAD_GRAYSCALE)
image2 = cv2.imread("9i2.jpg", cv2.IMREAD_GRAYSCALE)

assert image1.shape == image2.shape, "Input images must have the same dimensions"

averaged_image = cv2.addWeighted(image1, 0.5, image2, 0.5, 0)

cv2.imwrite("8o.jpg", averaged_image)
