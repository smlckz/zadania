from matplotlib import pyplot as plt
import cv2
import numpy as np

img = cv2.imread("1i.png", 0)
laplacian = cv2.Laplacian(img, -1, None, 3)

robert_x_kernel = np.array([[-1, 0], [0, 1]])
robert_y_kernel = np.array([[0, -1], [1, 0]])
robert_x = cv2.filter2D(img, -1, robert_x_kernel)
robert_y = cv2.filter2D(img, -1, robert_y_kernel)
robert_combined = cv2.bitwise_or(robert_x, robert_y)

prewitt_x_kernel = np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])
prewitt_y_kernel = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
prewitt_x = cv2.filter2D(img, -1, prewitt_x_kernel)
prewitt_y = cv2.filter2D(img, -1, prewitt_y_kernel)
prewitt_combined = cv2.bitwise_or(prewitt_x, prewitt_y)

sx = cv2.Sobel(img, cv2.CV_64F, 1, 0)
sy = cv2.Sobel(img, cv2.CV_64F, 0, 1)
sobel_x = np.uint8(np.absolute(sx))
sobel_y = np.uint8(np.absolute(sy))
sobel_combined = cv2.bitwise_or(sobel_x, sobel_y)

empty = np.zeros(img.shape, dtype=np.uint8)

data = [
    (img, "Original Image"),
    (laplacian, "Laplacian"),
    (robert_x, "Robert in x direction"),
    (robert_y, "Robert in y direction"),
    (robert_combined, "Combined Robert"),
    (prewitt_x, "Prewitt in x direction"),
    (prewitt_y, "Prewitt in y direction"),
    (prewitt_combined, "Combined Prewitt"),
    (sobel_x, "Sobel in x direction"),
    (sobel_y, "Sobel in y direction"),
    (sobel_combined, "Combined Sobel"),
    (empty, ""),
]

fig, axs = plt.subplots(3, 4, figsize=(10, 7))

for ax, (image, title) in zip(axs.flat, data):
    if image is not img:
        image = 255 - image
    ax.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
    ax.set_title(title, fontsize=10)
    ax.axis("off")

plt.tight_layout()
plt.savefig("14.svg")
