import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image, ImageFilter
img=cv2.imread("images/landscape.png")
img1=Image.open("images/landscape.png")
mean=cv2.blur(img,(3,3))
median=cv2.medianBlur(img,3)
w=cv2.filter2D(img,-1,3)
gau=cv2.GaussianBlur(img,(3,3),0)
minimg=np.array(img1.filter(ImageFilter.MinFilter(size=3)).convert('RGB'))
maximg=np.array(img1.filter(ImageFilter.MaxFilter(size=3)).convert('RGB'))
titles=["Original Image","Mean Image","Weighted Avg Image","Median Image","Min Image","Max Image"]
images=[img,mean,median,w,minimg,maximg]
for i in range(6):
    plt.subplot(2,3,i+1)
    plt.imshow(images[i][:,:,::-1])
    plt.title(titles[i])
    plt.xticks([])
    plt.yticks([])
plt.show()