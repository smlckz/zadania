import cv2
import numpy as np

image = cv2.imread("4i.jpg")

blue_channel, green_channel, red_channel = cv2.split(image)

zeros = np.zeros_like(blue_channel)

# Merge each channel with zeroes for the other channels
blue_image = cv2.merge([blue_channel, zeros, zeros])
green_image = cv2.merge([zeros, green_channel, zeros])
red_image = cv2.merge([zeros, zeros, red_channel])

cv2.imwrite("4.b.jpg", blue_image)
cv2.imwrite("4.g.jpg", green_image)
cv2.imwrite("4.r.jpg", red_image)
