import cv2
import numpy as np
import matplotlib.pyplot as plt
from skimage.util import random_noise

image = cv2.imread("4igr.jpg", cv2.IMREAD_GRAYSCALE)

# Add salt and pepper noise
salt_pepper_noise = random_noise(image, mode="s&p", amount=0.02)

# Add Gaussian noise
gaussian_noise = random_noise(image, mode="gaussian", mean=0, var=0.01)

# Create subplots
plt.figure(figsize=(10, 5))

# Salt and pepper noise
plt.subplot(1, 2, 1)
plt.imshow(salt_pepper_noise, cmap="gray")
plt.title("Salt and Pepper Noise")
plt.axis("off")

# Gaussian noise
plt.subplot(1, 2, 2)
plt.imshow(gaussian_noise, cmap="gray")
plt.title("Gaussian Noise")
plt.axis("off")

plt.savefig("11.svg")
