import cv2
import numpy as np

image = cv2.imread("4i.jpg", cv2.IMREAD_GRAYSCALE)

# a) Brightness enhancement
brightness_enhanced = cv2.add(image, 50)
cv2.imwrite("7.a.jpg", brightness_enhanced)

# b) Brightness suppression
brightness_suppressed = cv2.subtract(image, 50)
cv2.imwrite("7.b.jpg", brightness_suppressed)

# c) Contrast manipulation
alpha = 1.5
contrast_adjusted = cv2.multiply(image, alpha)
cv2.imwrite("7.c.jpg", contrast_adjusted)

# d) Gray level slicing without background
lower_threshold = 100
upper_threshold = 200
gray_level_sliced = np.copy(image)
gray_level_sliced[
    (gray_level_sliced >= lower_threshold) & (gray_level_sliced <= upper_threshold)
] = 255
cv2.imwrite("7.d.jpg", gray_level_sliced)
