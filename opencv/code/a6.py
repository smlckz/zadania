import cv2
import matplotlib.pyplot as plt

image = cv2.imread("4i.jpg", cv2.IMREAD_GRAYSCALE)

# Perform histogram equalization
equalized_image = cv2.equalizeHist(image)

# Calculate histograms
hist_original = cv2.calcHist([image], [0], None, [256], [0, 256])
hist_equalized = cv2.calcHist([equalized_image], [0], None, [256], [0, 256])

# Plot original and equalized images and their histograms
plt.figure(figsize=(10, 8))

# Original image and histogram
plt.subplot(2, 2, 1)
plt.imshow(image, cmap="gray")
plt.title("Original Image")
plt.xticks([])
plt.yticks([])

plt.subplot(2, 2, 2)
plt.plot(hist_original, color="black")
plt.title("Histogram of Original Image")
plt.xlabel("Pixel Value")
plt.ylabel("Frequency")

# Equalized image and histogram
plt.subplot(2, 2, 3)
plt.imshow(equalized_image, cmap="gray")
plt.title("Equalized Image")
plt.xticks([])
plt.yticks([])

plt.subplot(2, 2, 4)
plt.plot(hist_equalized, color="black")
plt.title("Histogram of Equalized Image")
plt.xlabel("Pixel Value")
plt.ylabel("Frequency")

plt.tight_layout()
plt.savefig("6.svg")
