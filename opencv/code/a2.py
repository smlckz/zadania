import cv2
import numpy as np

image = cv2.imread("1i.png")

angle = float(input("Enter angle in degrees: "))

height, width = image.shape[:2]

rotation_matrix = cv2.getRotationMatrix2D((width / 2, height / 2), angle, 1)

# Determine the new dimensions of the rotated image
cos_theta = np.abs(rotation_matrix[0, 0])
sin_theta = np.abs(rotation_matrix[0, 1])
new_width = int(height * sin_theta + width * cos_theta)
new_height = int(height * cos_theta + width * sin_theta)

# Adjust the rotation matrix to take into account translation to keep the entire image within bounds
rotation_matrix[0, 2] += (new_width - width) / 2
rotation_matrix[1, 2] += (new_height - height) / 2

rotated_image = cv2.warpAffine(image, rotation_matrix, (new_width, new_height))

cv2.imwrite(f"2.({angle}).png", rotated_image)
cv2.waitKey(0)
cv2.destroyAllWindows()
