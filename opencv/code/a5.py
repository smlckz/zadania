import cv2
import numpy as np

image = cv2.imread("4i.jpg", cv2.IMREAD_GRAYSCALE)

# Image Negative
negative_image = 255 - image
cv2.imwrite("5.negative.jpg", negative_image)

# Log Transformation
c = 255 / np.log(1 + np.max(image))
log_transformed = c * np.log1p(1.0 + image)
log_transformed = np.uint8(log_transformed)
cv2.imwrite("5.log.jpg", log_transformed)

# Power Law Transform
gamma = 0.5
power_law_transformed = np.power(image, gamma)
power_law_transformed = cv2.normalize(
    power_law_transformed, None, 0, 255, cv2.NORM_MINMAX
)
power_law_transformed = np.uint8(power_law_transformed)
cv2.imwrite("5.power_law.jpg", power_law_transformed)


# Piecewise Linear Transform
def piecewise_linear(x):
    return np.piecewise(
        x,
        [x < 50, (x >= 50) & (x < 100), (x >= 100) & (x < 150), x >= 150],
        [
            lambda x: 0,
            lambda x: 255 * ((x - 50) / (100 - 50)),
            lambda x: 255,
            lambda x: 255 * ((255 - x) / (255 - 150)),
        ],
    )


piecewise_transformed = piecewise_linear(image)
piecewise_transformed = np.uint8(piecewise_transformed)
cv2.imwrite("5.piecewise.jpg", piecewise_transformed)
