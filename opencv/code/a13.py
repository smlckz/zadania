import cv2

image = cv2.imread("1i.png", cv2.IMREAD_GRAYSCALE)

threshold_values = [50, 100, 150]

for i, threshold in enumerate(threshold_values, start=1):
    _, segmented_image = cv2.threshold(image, threshold, 255, cv2.THRESH_BINARY)
    cv2.imwrite(f"13.{i}.jpg", segmented_image)
