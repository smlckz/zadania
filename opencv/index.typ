#import "tpl.typ": *
#show: body => apply(body)
#let semibold(body) = text(weight: 600, body)
#text(size: 1.3em, align(center)[#semibold[Practical Assignments] \ _on_ \ #semibold[Image Processing Lab]])
#set enum(full: true, numbering: (..args) => {
  let patterns = ("1.", "a)")
  let pattern = patterns.at(calc.min(args.pos().len(), patterns.len()) - 1)
  numbering(pattern, args.pos().last())
})

#set raw(lang: "python")
#set par(justify: true)
#set text(size: 10pt)


+ Write a program to do image format conversion i.e., from RGB to gray, gray to binary, RGB to binary, RGB to HSV, HSV to RGB, RGB to YCbCr and YCbCr to RGB.
+ Write a program to read an image and rotate that image in clockwise and anti-clockwise direction, and display it.
+ Write a program to find the histogram of a gray image and display the histogram.
+ Write a program to perform color separation into R, G, and B from an color image.
+ Write a program to enhance the image in spatial domain using
  + Image negative
  + Log transformation
  + Power law transform
  + Piecewise linear transform
+ Write a program to enhance the image in spatial domain using histogram equalization method.
+ Write a program to perform the following image enhancement methods:
  + Brightness enhancement
  + Brightness suppression
  + Contrast manipulation
  + Gray level slicing without background 
+ Write a program to average two images together into a single image. Display the new image.
+ Write a program to compare two images using image subtraction.
+ Write a program to perform the following image arithmetic.
  + Image addition
  + Image subtraction
  + Image multiplication
  + Image division
+ Write a program to add various types of noise (salt and pepper noise, Gaussian noise) to an image.
+ Write a program to enhance an image using mean filtering, weighted average filtering, median filtering, max/min filtering.
+ Write a program to segment an image using thresolding technique.
+ Write a program to segment an image based on 
  + Region growing
  + Region splitting
  + Region merging
+ Write a program to find the edge of a given image with the following operators.
  + Difference operator
  + Robert operator
  + Prewitt operator
  + Sobel operator
+ Write a program to read two images `lena.bin` and `peppers.bin`. define a new 256 x 256 image J as follows : the left half of J i.e., the first 128 columns, should be equal to the left half of lena image and the right half of J, i.e., the 129th column through the 256th column should be equal to the right half of pepper image. Show J image.

