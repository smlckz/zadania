#import "/tpl.typ": *
#show: A => apply(A)
#set raw(lang: "py")
#set par(leading: 0.6em)
#set figure.caption(position: top)
#set figure(supplement: none, numbering: none)

#assignment(1)[
  Write a program to do image format conversion i.e., from RGB to gray, gray to binary, RGB to binary, RGB to HSV, HSV to RGB, RGB to YCbCr and YCbCr to RGB.
]

#scos(1)[
  #{
    let list-of-images = (image("/images/1i.png", width: 80%), )
    for i in range(1, 8) {
      list-of-images.push(image("/images/1." + str(i) + ".png", width: 80%))
    }
    let captions = ("Original Image", [RGB #sym.arrow.r Gray], [Gray #sym.arrow.r Binary], [RGB #sym.arrow.r Binary], [RGB #sym.arrow.r HSV], [HSV #sym.arrow.r RGB], [RGB #sym.arrow.r YCbCr], [YCbCr #sym.arrow.r RGB])
    let figures = ()
    for i in range(list-of-images.len()) {
      figures.push(figure(caption: captions.at(i), list-of-images.at(i)))
    }
    grid(
      columns: 2,
      gutter: 1em,
      ..figures
    )
  }
]

#signature()
#colbreak()

#assignment(2)[
  Write a program to read an image and rotate that image in clockwise and anti-clockwise direction, and display it.
]

#scos(2)[
  #figure(caption: [Original image], image("/images/1i.png", width: 50%))
  #figure(caption: [Rotated 60#sym.degree anti-clockwise], image("/images/2.(60.0).png", width: 50%))
  #figure(caption: [Rotated 45#sym.degree clockwise], image("/images/2.(-45.0).png", width: 60%))
]

#signature()
#colbreak()

#assignment(3)[
  Write a program to find the histogram of a gray image and display the histogram.
]

#scos(3)[
  #image("/images/3.svg")
]

#signature()
#colbreak()

#assignment(4)[
  Write a program to perform color separation into R, G, and B from an color image.
]

#scos(4)[
  #{
    let filenames = ("4i", "4.r", "4.g", "4.b")
    let list-of-images = ()
    for i in range(filenames.len()) {
      list-of-images.push(image("/images/" + filenames.at(i) + ".jpg", width: 80%))
    }
    let captions = ("Original Image", "Red Channel", "Green Channel", "Blue Channel")
    let figures = ()
    for i in range(list-of-images.len()) {
      figures.push(figure(caption: captions.at(i), list-of-images.at(i)))
    }
    grid(
      columns: 2,
      gutter: 1em,
      ..figures
    )
  }
]

#signature()
#colbreak()

#assignment(5)[
  Write a program to enhance the image in spatial domain using
  + Image negative
  + Log transformation
  + Power law transform
  + Piecewise linear transform
]

#scos(5)[
  #{
    let filenames = ("negative", "log", "power_law", "piecewise")
    let list-of-images = ()
    for i in range(filenames.len()) {
      list-of-images.push(image("/images/5." + filenames.at(i) + ".jpg", width: 100%))
    }
    let captions = ("Image negative", "Log transformation", "Power law transform", "Piecewise linear transform")
    let figures = ()
    for i in range(list-of-images.len()) {
      figures.push(figure(caption: captions.at(i), list-of-images.at(i)))
    }
    grid(
      columns: 2,
      gutter: 1em,
      ..figures
    )
  }  
]

#signature()
#colbreak()

#assignment(6)[
  Write a program to enhance the image in spatial domain using histogram equalization method.
]

#scos(6)[
  #image("/images/6.svg")
]

#signature()
#colbreak()

#assignment(7)[
  Write a program to perform the following image enhancement methods:
  + Brightness enhancement
  + Brightness suppression
  + Contrast manipulation
  + Gray level slicing without background 
]

#scos(7)[
  #{
    let filenames = ("a", "b", "c", "d")
    let list-of-images = ()
    for i in range(filenames.len()) {
      list-of-images.push(image("/images/7." + filenames.at(i) + ".jpg", width: 70%))
    }
    let captions = (
      "Brightness enhancement",
      "Brightness suppression",
      "Contrast manipulation",
      "Gray level slicing without background"
    )
    let figures = ()
    for i in range(list-of-images.len()) {
      figures.push(figure(caption: captions.at(i), list-of-images.at(i)))
    }
    grid(
      columns: 2,
      gutter: 1em,
      ..figures
    )
  }  
]

#signature()
#colbreak()

#assignment(8)[
  Write a program to average two images together into a single image.
]

#scos(8)[
  #{
    let filenames = ("9i1", "9i2", "8o")
    let list-of-images = ()
    for i in range(filenames.len()) {
      list-of-images.push(image("/images/" + filenames.at(i) + ".jpg", width: 70%))
    }
    let captions = (
      "Image 1",
      "Image 2",
      "Average"
    )
    let figures = ()
    for i in range(list-of-images.len()) {
      figures.push(figure(caption: captions.at(i), list-of-images.at(i)))
    }
    grid(
      columns: 2,
      gutter: 1em,
      ..figures
    )
  }
]

#signature()
#colbreak()

#assignment(9)[
  Write a program to compare two images using image subtraction.
]

#scos(9)[
  #{
    let filenames = ("9i1", "9i2", "9o")
    let list-of-images = ()
    for i in range(filenames.len()) {
      list-of-images.push(image("/images/" + filenames.at(i) + ".jpg", width: 70%))
    }
    let captions = (
      "Image 1",
      "Image 2",
      "Difference"
    )
    let figures = ()
    for i in range(list-of-images.len()) {
      figures.push(figure(caption: captions.at(i), list-of-images.at(i)))
    }
    grid(
      columns: 2,
      gutter: 1em,
      ..figures
    )
  }
]

#signature()
#colbreak()

#assignment(10)[
  Write a program to perform the following image arithmetic.
  + Image addition
  + Image subtraction
  + Image multiplication
  + Image division
]

#scos(10, cont: true)[
  #image("/images/10.svg")
]

#signature()
#colbreak()

#assignment(11)[
  Write a program to add various types of noise (salt and pepper noise, Gaussian noise) to an image.
]

#scos(11, cont: true)[
  #image("/images/11.svg")
]

#signature()
#colbreak()

#assignment(12)[
  Write a program to enhance an image using mean filtering, weighted average filtering, median filtering, max/min filtering.
]

#scos(12, cont: true)[
  #image("/images/12.svg")
]

#signature()
#colbreak()

#assignment(13)[
  Write a program to segment an image using thresolding technique.
]

#scos(13)[
  #{
    let filenames = ("1igr.png", "13.1.jpg", "13.2.jpg", "13.3.jpg")
    let list-of-images = ()
    for i in range(filenames.len()) {
      list-of-images.push(image("/images/" + filenames.at(i), width: 70%))
    }
    let captions = (
      "Original Image",
      "Segment 1",
      "Segment 2",
      "Segment 3",
    )
    let figures = ()
    for i in range(list-of-images.len()) {
      figures.push(figure(caption: captions.at(i), list-of-images.at(i)))
    }
    grid(
      columns: 2,
      gutter: 1em,
      ..figures
    )
  }
]

#signature()
#colbreak()

#assignment(14)[
  Write a program to find the edge of a given image with the following operators.
  + Difference operator
  + Robert operator
  + Prewitt operator
  + Sobel operator
]

#scos(14)[
  #image("/images/14.svg")
]

#signature()


