#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.75em)

#assignment(24, block: true)[
  Write a program that prompts the user for an integer input, and if the entered value is less than zero, throw a custom exception; additionally, handle exceptions for input of non-integer data types.
]

#scos("CustomExceptionExample")

=== Discussion

#skind[Classes and methods implemented in the program]

- Class `NegativeNumberException` (extends `Exception`): Custom exception class for negative numbers.
  - Constructor `public NegativeNumberException(String)`: Constructs an Exception object to be thrown using the provided message string.

- Main program:
  - Class `CustomExceptionExample`:
    - `public static void main(String[])`: Contains the main program demonstrating custom exception handling for negative numbers and handling input mismatches.

#signature()
