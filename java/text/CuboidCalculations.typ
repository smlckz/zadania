#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.7em)

#assignment(16)[
  Write a program to create a base class named `Rectangle` and another class named `Cuboid` deriving `Rectangle` overloading the constructors and print surface area and volume of a `Cuboid` object.
]
#v(2em)
#scos("CuboidCalculations")

=== Discussion

#skind[Classes and methods implemented in the program]

- The `Rectangle` class:
  - `int area()`: Calculates the area of a rectangle based on its length and breadth.

- The `Cuboid` class extends `Rectangle`:
  - `int surfaceArea()`: Calculates the surface area of a cuboid based on its length, breadth, and height.
  - `int volume()`: Calculates the volume of a cuboid based on its length, breadth, and height.

- The `CuboidCalculations` class contains the main program:
  - `public static void main(String[])`: Creates an instance of `Cuboid`, calculates and displays its surface area and volume.
  
#signature()
