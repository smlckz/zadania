#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.6em)

#assignment(23, block: true)[
  Write a program that defines an interface with three methods in the `pkgOne` package, implements the interface in a class within the `pkgTwo` package, and from the `main()` method in the working directory, instantiate the class and invoke the interface methods.
]

#scos("PackageInterfaceExample", include-before: ("pkgOne/MyInterface", "pkgTwo/MyClass"))

=== Discussion

#skind[Interfaces, classes and methods implemented in the program]

- Package `pkgOne`:
  - Interface `MyInterface`:
    - `void methodOne()`: An interface method.
    - `void methodTwo()`: An interface method.
    - `void methodThree()`: An interface method.

- Package `pkgTwo`:
  - Class `MyClass` (implements `pkgOne.MyInterface`):
    - `public void methodOne()`: Implementation of `methodOne` from `MyInterface`.
    - `public void methodTwo()`: Implementation of `methodTwo` from `MyInterface`.
    - `public void methodThree()`: Implementation of `methodThree` from `MyInterface`.

- Main program:
  - Class `PackageInterfaceExample`:
    - `public static void main(String[])`: Contains the main program creating an object of `MyClass` (which implements `MyInterface`) and calling its methods.

#signature()
