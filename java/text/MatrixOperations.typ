#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.6em)

#assignment(4)[
  Write a menu-driven program to perform addition, subtraction and multiplication operations on matrix objects.
]

#scos("MatrixOperations")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.util.Scanner` class:
  - `float nextFloat()`: Scan a `float` value from the the input stream.
- `java.util.function.BinaryOperator<T>` Interface:
  - Objects of this interface represent operations upon two operands of the same type, producing a result of the same type as the operands.
  - This is a subinterface of `BiFunction<T, T, T>` whose functional method is `T apply(T, T)`.
- `java.lang.IllegalArgumentException` class represents the exception to be thrown when an illegal argument is provided to a function.
- `java.util.ArrayList<T>` class provides a resizeable array from the Java Collections framework.
  - `T get(int)`: Returns the value at the specified position in the list.
  - `void add(T)`: Appends the specified element to the end of the list.
- `java.util.InputMismatchException` is thrown by the methods of `Scanner` class when encountering invalid input.
  
#skind[Classes and methods implemented in the program]

- The `Matrix` class provides sufficient interface to operate on a matrix of `float` values.
  - Constructor `Matrix(int, int)`: Creates the underlying two dimensional array with the given matrix dimensions.
  - `int rows(), int cols()`: Returns the number of rows and columns of the matrix, respectively.
  - `float get(int, int)`: Returns the value at the specified position.
  - `void set(int, int, float)`: Sets the value at the specified position.
  - `void input(Scanner)`: Take the elements of the matrix from the user using the provided `Scanner` instance.
  - `void display()`: Displays the matrix elements in a tabular format.
- The `MatrixOperations` class implements the matrix operations as static methods.
  - `static Matrix add(Matrix, Matrix) throws IllegalArgumentException`,
  - `static Matrix subtract(Matrix, Matrix) throws IllegalArgumentException`,
  - `static Matrix multiply(Matrix, Matrix) throws IllegalArgumentException`:
    Perform the respective operations on the provided two matrices, taking care of the compatibility to the operations.
- The `MatrixOperationsCLI` orchestrates the main program with:
  - `static void menu()`: Displays the menu.
  - `static Matrix takeMatrix(Scanner)`: Takes requisite user input to create a new matrix object.
  - `static void displayMatrix(Scanner, ArrayList<Matrix>)`: Asks the user to choose the matrix to display among the available matrices and displays it.
  - `static void operate(Scanner, ArrayList<Matrix>, String, BinaryOperator<Matrix>)`: Performs the specified operation on the matrices chosen by the user.
  - `public static void main(String[])`: Implements a menu-driven program for the matrix operations.
#signature()
