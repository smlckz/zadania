#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.475em)

#assignment(1)[
  Write a menu-driven program to implement linear and binary search on an object of a custom array class.
]

#scos("ArraySearch")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `void java.util.Arrays.sort(double[])`: A static method facilitating in-place sorting of the provided array.
- `java.util.Scanner` Class:
  - Constructor `Scanner(java.io.InputStream)`: Creates a scanner object for scanning values from given input stream.
  - Methods `int nextInt()`, `double nextDouble()`: Scan an integer or `double` value from the the input stream.
- `java.util.function.BiFunction<T, U, V>` Interface:
  - Objects implementing this interface represent functions taking two arguments of types `T` and `U`, returning a value of type `V`.
  - Member method `V apply(T, U)` is used to invoke the underlying function.
- `public static java.io.PrintStream System.out, System.err`: Represents the standard output and error streams.
- `public void java.io.PrintStream.println(String)` and `public void java.io.PrintStream.print(String)`: Print text to the print stream, with or without newline at the end, respectively.

#skind[Classes and methods implemented in the program]

- The `Array` class acts as an array with essential methods:
  - Constructor `Array()`: Invokes the `takeInput()` method.
  - `void takeInput()`: Takes the array length and elements of the array from the user.
  - `void display()`: Displays the array elements.
  - `double get(int)`: Returns the value at the specified position.
  - `void set(int, double)`: Sets the value at the specified position.
  - `void sort()` Utilizes `Arrays.sort` for in-place sorting.
  - `int size()`: Returns the size of the array.
- The `ArrayOperations` class includes methods for performing searching operations, which takes the array and the value to search in it and returns the position of the element if it found in the array or `-1` instead.
  - `static int linearSearch(Array, double)`: Performs linear search on the given `Array`.
  - `static int binarySearch(Array, double)`: Conducts binary search; the array must be sorted before calling.
- The `ArraySearch` orchestrates the main program with:
  - `static void menu()`: Displays the menu.
  - `static void performSearch(Scanner, Array, BiFunction<Array, Double, Integer>)`: Takes user input, performs the specified search, and displays the result.
  - `public static void main(String[])`: Implements a menu-driven program for array searching.

#signature()
