#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.65em)

#assignment(22, block: true)[
  Create a program that defines a class with variables and methods in the `pOne` package, extends this class in another package `pTwo`, and within the method of the `pTwo` class, access the variables and methods of the original class in `pOne`; finally, from the `main()` method in the working directory, access the members of the second class.
]

#scos("PackageExample", include-before: ("pOne/ClassOne", "pTwo/ClassTwo"))

=== Discussion

#skind[Classes and methods implemented in the program]

- Package `pOne`:
  - Class `ClassOne`:
    - `protected int variableOne`: A protected variable.
    - `public void methodOne()`: A public method.

- Package `pTwo`:
  - Class `ClassTwo` (extends `pOne.ClassOne`):
    - `public void methodTwo()`: A public method that calls `methodOne()` and accesses `variableOne` from `ClassOne`.

- Class `PackageExample`:
  - `public static void main(String[])`: Contains the main program creating an object of `ClassTwo` and calling `methodTwo()`.

#signature()
