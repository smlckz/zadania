#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.55em)

#assignment(3)[
  Write a menu-driven program to create a class with a String member variable and perform basic string operations:
  #box(align(left, [
  - count the number of words,
  - case-insensitively check whether the string is palindrome, and
  - reverse the string.]))
]

#scos("StringOperations")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.util.Scanner` Class:
  - Method `String nextLine()`: Scan a line of text as a string from the the input stream.
- `java.lang.String` Class:
  - `int length()` method: Returns the length of the string.
  - `char charAt(int)`: Returns the character in the string at the provided position.
  - `char[] toCharArray()`: Returns an array of characters corresponding to the string.
  - `static String copyValueOf(char[])`: Creates a new string from the provided character array.
  
#skind[Classes and methods implemented in the program]

- `MyString` Class:
  Provides the methods implementing the specified operations.
  - `String str` is the underlying string object upon which the operations are to be performed.
  - Constructor `MyString(String)`: Create an object of this class for the provided string.
  - `String valueOf()`: Returns the underlying string object.
  - `int countWords()`: Counts the words in the underlying string manually by  iterating over the characters in the string.
  - `MyString reverse()`: Returns a new object of MyString with the underlying string reversed manually by iterating over characters.
  - `MyString toLowerCase()`: Returns a new object of MyString with its characters manually converted to lower-case.
  - `boolean equals(MyString)`: Check whether the underlying strings are equal by comparing the arrays of characters.
  - `boolean isCaseInsensitivePalindrome()`: Checks whether the underlying string is a palindrome case-insensitively using the `reverse()`, `toLowerCase()` and `equals()` method.
- The `StringOperations` orchestrates the main program with:
  - `static void menu()`: Displays the menu.
  - `public static void main(String[])`: Implements a menu-driven program for the specified string operations.


#signature()
