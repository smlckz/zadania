#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.5em)

#assignment(15, block: true)[
  Write a program to create a class `Parent` having instance variables `id`, `name` and `address`; a class `ChildOne` having instance variables `id`, `name`, `address` and `marks`; another class `ChildTwo` with instance variables `id`, `name`, `address`, `qualification` and `salary`. Design the program using `super` call with proper parameters within each class and define your own method to display values of the member variable and use an object of each class from `main()` to display their properties.
]

#scos("ParentWithTwoChildren")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.lang.Object` class:
  - `Class<?> getClass()`: Returns the `Class` object representing the class of the object.
- `java.lang.Class<T>` class:
  - `String getSimpleName()`: Returns the simple name of the underlying class as a string.

#skind[Classes and methods implemented in the program]

- `Parent`: Base class with `id`, `name`, and `address`, displaying all information dynamically using the `display` method.
- `ChildOne`: Inherits from `Parent`, adds `marks`, and displays them after parent data using the `display` method.
- `ChildTwo`: Inherits from `Parent`, adds `qualification` and `salary`, and displays them after parent data using the `display` method.
- `Main`: Creates and showcases `Parent`, `ChildOne`, and `ChildTwo` object details using the `main` method.

#signature()
