#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.75em)

#assignment(9)[
  Write a program to show that `static` blocks get executed before any object creation and implement the use of static variable to count the number of objects.
]
#v(-1em)
#scos("StaticExecution")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.util.Random` class:
  Provides methods for generating pseudorandom numbers.
  - `int nextInt(int)`: Returns a pseudorandom, uniformly-distributed, random number between 0 (inclusive) and provided value (exclusive).

#skind[Classes and methods implemented in the program]

- `StaticExample` class:
  The class being used to demonstrate the order of execution of static block.
  - `static int getObjectCount()`: Returns the object count of this class.
  - Constructor `StaticExample()`: Shows a message first time it is called, i.e. when the first object is created. Increments the object count static variable.
- `StaticExecution` class contains the main method of the program.
  - `public static void main(String[])`: Creates a random number of objects of the `StaticExample` class and shows the number of objects created.

#signature()
