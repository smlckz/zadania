#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.7em)

#assignment(18, block: true)[
  Write a program to create a class named `CircularBase` containing a method `getArea()` to calculate the base area, an interface named `_3dShape` with two methods `calArea()` and `calVolume()`, two subclasses of `CircularBase` named `Cone` and `Cylinder` implementing `_3dShape`; calculate the base area and volume of these shapes with the help of `calArea()` method.
]

#scos("CircularBaseExample")

=== Discussion

#skind[Interfaces, classes and methods implemented in the program]

- The `ThreeDimensionalShape` interface:
  - `double calArea()`: Calculates the surface area of a three-dimensional shape.
  - `double calVolume()`: Calculates the volume of a three-dimensional shape.

- The `CircularBase` class:
  - Constructor `CircularBase(double)`: Initializes a shape with a circular base using the provided radius.
  - `double getArea()`: Calculates the area of the circular base.

- The `Cone` class extends `CircularBase` and implements `ThreeDimensionalShape`:
  - Constructor `Cone(double, double)`: Initializes a cone with a circular base, taking radius and height.
  - `double calArea()`: Overrides the interface method to calculate the total surface area of the cone.
  - `double calVolume()`: Overrides the interface method to calculate the volume of the cone.

- The `Cylinder` class extends `CircularBase` and implements `ThreeDimensionalShape`:
  - Constructor `Cylinder(double, double)`: Initializes a cylinder with a circular base, taking radius and height.
  - `double calArea()`: Overrides the interface method to calculate the total surface area of the cylinder.
  - `double calVolume()`: Overrides the interface method to calculate the volume of the cylinder.

- The `CircularBaseExample` class contains the main program:
  - `public static void main(String[])`: Takes user input for the dimensions of a cone and a cylinder, creates instances of these shapes, and displays their base area, total surface area, and volume.
  
#signature()
