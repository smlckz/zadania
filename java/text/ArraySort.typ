#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")

#assignment(2)[
  Write a menu-driven program to implement bubble sort and selection sort on an object of a custom array class.
]

#scos("ArraySort")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.util.function.Consumer<T>` Interface:
  - Objects implementing this interface represent functions taking an argument of type `T`, returning nothing, effectively consuming the provided value.
  - Member method `void accept(T)` is used to invoke the underlying function.

#skind[Classes and methods implemented in the program]

- The `ArrayOperations` class includes methods for performing sorting operations in-place.
  - `static void bubbleSort(Array)`: Performs bubble sort on the given `Array`.
  - `static void selectionSort(Array)`: Performs selection sort on the given `Array`.
- The `ArraySort` orchestrates the main program with:
  - `static void menu()`: Displays the menu.
  - `static void performSort(Scanner, Array, Consumer<Array>)`: Performs the specified sort, and displays the array before and after the sorting operation.
  - `public static void main(String[])`: Implements a menu-driven program for array sorting.


#signature()
