#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.7em)

#assignment(19, block: true)[
  Write a program to create a class containing an inner class to show that inner class can use members of outer class directly, but the outer class can use members of inner class only through its objects. Check the name of the inner class file created when it was compiled.
]
#v(-1em)
#scos("InnerClassExample")

=== Discussion

#skind[Classes and methods implemented in the program]

- The `OuterClass` class:
  - Private field `int outerMember`: Represents a member variable in the outer class.
  - The `InnerClass` class:
    - Private field `int innerMember`: Represents a member variable in the inner class.
    - Method `void accessMembers()`: Prints the values of both outer and inner members.
  - Method `void accessInnerMembers()`: Creates an instance of the inner class and prints the value of the inner member through the object.

- The `InnerClassExample` class contains the main program:
  - `public static void main(String[])`: Demonstrates the usage of outer and inner classes by creating instances and accessing their members.

/ Note : The file name of the (bytecode) compiled class file corresponding to the inner class is ```text OuterClass$InnerClass.class```.
  
#signature()
