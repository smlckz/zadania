#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.7em)

#assignment(8, pad: true)[
  Write a program to find a number from an array of number objects.
]

#scos("FindNumberInArray")

#v(1em)

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.util.Scanner` class:
  - `int nextInt()`: Scan an `int` value from the input stream.
  - `double nextDouble()`: Scan a `double` value from the input stream.
- `java.util.InputMismatchException` is thrown by the methods of `Scanner` class when encountering invalid input.

#skind[Classes and methods implemented in the program]

- The `Number` class objects represent a `double` value and provide methods for equality comparison.
  - Constructor `Number(double)`: Create a number object for the provided value.
  - `double valueOf()`: Returns the underlying number.
  - `boolean equals(Object)`: Compares the equality of the number with another object.
- The `NumberArray` class manages an array of `Number` objects.
  - Constructor `NumberArray(double[])`: Create a `NumberArray` object from an array of `double` values.
  - `int find(Number)`: Find the position of a given `Number` in the array, or return -1 if not found.
  - `void display()`: Display the elements of the array.
- The `FindNumberInArray` class contains the main program for finding a number in an array:
  - `public static void main(String[])`: Takes user input for the length and elements of an array, creates a `NumberArray`, and finds a specified number in the array.
  
#signature()
