#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.6em)

#assignment(14, block: true)[
  Write a program to create an abstract class `Shape` with two abstract methods, `area()` and `display()` and make three concrete derived classes `Rectangle`, `Circle` and `Triangle` which can calculate area and display them seperately.
]

#scos("AbstractShapeCalculations")

=== Discussion

#skind[Classes and methods implemented in the program]

- The `Shape` abstract class:
  - `abstract double area()`: To be implemented by subclasses for calculating the area.
  - `abstract void display()`: To be implemented by subclasses for displaying shape details.

- The `Rectangle` class extends `Shape`:
  - `double area()`: Calculates the area of a rectangle using its length and width.
  - `void display()`: Displays details of the rectangle.

- The `Circle` class extends `Shape`:
  - `double area()`: Calculates the area of a circle using its radius.
  - `void display()`: Displays details of the circle.

- The `Triangle` class extends `Shape`:
  - `double area()`: Calculates the area of a triangle using its side lengths.
  - `double perimeter()`: Calculates the perimeter of a triangle.
  - `void display()`: Displays details of the triangle.

- The `AbstractShapeCalculations` class contains the main program:
  - `public static void main(String[])`: Implements a menu-driven program for calculating and displaying areas of different shapes (Rectangle, Circle, Triangle) based on user input. Utilizes abstract classes and polymorphism for handling various shapes and their respective calculations.

#signature()
