#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.4em)

#assignment(10, reduce-gap: true, pad: true)[
  Write a program to implement a singleton class.
]
#v(-1em)
#scos("SingletonExample")

=== Discussion

#skind[Classes and methods implemented in the program]

- The `Singleton` class implements the Singleton design pattern to ensure a single instance of the class is created.
  - `private static Singleton instance`: Private static variable to hold the single instance of the class.
  - `private Singleton()`: Private constructor to prevent external instantiation and prints a message when the instance is created.
  - `public static Singleton getInstance()`: Public method to retrieve the singleton instance. If the instance does not exist, it creates one and prints a message.
  
- The `SingletonExample` class contains the main program to demonstrate the Singleton pattern:
  - `public static void main(String[])`: Creates two instances of `Singleton` using the `getInstance()` method and checks if they refer to the same instance.
    - If `s1` and `s2` reference the same object, it prints "The singleton instances are identical."  
#signature()
