#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.6em)

#assignment(17, block: true)[
  Write a program to create a class `Employee` with instance variables `name` and `id`; a subclass of `Employee` named `Scientist` with instance variables `no_of_publication` and `experience`; and its subclass named `DScientist` with instance variable `award`; implement the `public String toString() { }` method in each class to describe about its object with the member variables and from `main()` method create an object of each class and print each object.
]

#scos("EmployeeDisplay")

=== Discussion

#skind[Classes and methods implemented in the program]

- The `Employee` class:
  - Constructor `Employee(int, String)`: Initializes an employee with ID and name.
  - `public String toString()`: Returns a string representation of the employee details.

- The `Scientist` class extends `Employee`:
  - Constructor `Scientist(int, String, int, int)`: Initializes a scientist with additional attributes like the number of publications and years of experience.
  - `public String toString()`: Returns a string representation of the scientist details.

- The `DScientist` class extends `Scientist`:
  - Constructor `DScientist(int, String, int, int, String[])`: Initializes a distinguished scientist with additional awards.
  - `public String toString()`: Returns a string representation of the distinguished scientist details.

- The `EmployeeDisplay` class contains the main program:
  - `public static void main(String[])`: Creates instances of different employee types (Employee, Scientist, Distinguished Scientist) and displays their details.
  
#signature()
