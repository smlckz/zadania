#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.5em)

#assignment(12, block: true)[
  Write a program wherein design a class to represent a bank account. Include the following:
  #pad(left: 1em)[
  / Fields :
    - Name of the depositor
    - Address of the depositor
    - Account number
    - Balance amount in the account
  / Methods :
    - To assign initial values
    - To deposit an amount
    - To withdraw an amount after checking balance
    - To display the name, address and balance of a customer.
  ]
  From `main()` create object and call these methods.
]

#scos("BankAccountExample")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.util.ArrayList<E>` class provides a dynamic array implementation, allowing for the flexible storage and manipulation of elements in a resizable list.
  - `boolean add(E)`: Adds the specified element to the end of the list.
  - `E remove(int)`: Removes the element at the specified position in the list.
  - `int size()`: Returns the number of elements in the list.
- `java.util.Scanner` class:
  - `String nextLine()`: Scan a line of text from the input stream.
  - `long nextLong()`: Scan a `long` value from the input stream.
  - `double nextDouble()`: Scan a `double` value from the input stream.

#skind[Classes and methods implemented in the program]

- The `BankAccount` class represents a bank account with details like name, address, account number, and balance.
  - Constructor `BankAccount(String, String, long, double)`: Create a bank account with the given details.
  - `static BankAccount takeInput(Scanner)`: Takes input for a bank account from the user.
  - `long getAccountNo()`: Returns the account number of the bank account.
  - `double getBalance()`: Returns the balance of the bank account.
  - `boolean equals(Object)`: Compares bank accounts based on account number.
  - `void deposit(double)`: Deposits money into the account.
  - `static String formatBalance(double)`: Formats a balance value for display.
  - `void withdraw(double) throws Exception`: Withdraws money from the account, handling exceptions.
  - `void display()`: Displays the details of the bank account.
  - `static BankAccount lookup(ArrayList<BankAccount>, long)`: Looks up a bank account in the list based on account number.

- The `BankAccountExample` class contains the main program for interacting with bank accounts through a menu:
  - `static void menu()`: Displays a menu of operations.
  - `static BankAccount findAccount(Scanner, ArrayList<BankAccount>)`: Finds a bank account based on user input.
  - `public static void main(String[])`: Implements a menu-driven program for managing bank accounts, allowing creation, display, deposit, withdrawal, and closure of accounts.
  
#signature()
