#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.5em)

#assignment(6)[
  Write a program to find the surface area and volume of a cylinder using constructor by taking keyboard input or command-line input.
]

#scos("CylinderCalculations")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.lang.Double` class:
  - `static double parseDouble(String)`: Try to parse the provided string as a `double` value.
- `java.util.Scanner` class:
  - `double nextDouble()`: Scan a `double` value from the the input stream.
- `java.util.InputMismatchException` is thrown by the methods of `Scanner` class when encountering invalid input.
- `java.lang.NumberFormatExeption` is thrown by `parseDouble` method when the provided string does not constitute a valid `double` value.
  
#skind[Classes and methods implemented in the program]

- The `Cylinder` class objects represents a cylinder, a 3D geometric shape.
  - Constructor `Cylinder(double, double)`: Create a cylinder object of the provided radius and height.
  - `double volume()`: Returns the volume of the cylinder.
  - `double surfaceArea()`: Returns the surface area of the cylinder.
  - `void display()`: Display information about the cylinder.
- The `CylinderCalculationsCLI` orchestrates a main program with:
  - `public static void main(String[])`: Creates a cylinder object with its radius and height given as command-line arguments.
- The `CylinderCalculationsScan` orchestrates a main program with:
  - `public static void main(String[])`: Creates a cylinder object with its radius and height taken from user using `java.util.Scanner`.
#signature()
