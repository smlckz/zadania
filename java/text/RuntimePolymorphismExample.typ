#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.7em)

#assignment(21)[
  Write a program, wherein create an Interface and create two sub-classes implementing the interface to show the functionalities of runtime polymorphism (_a.k.a._ dynamic method dispatch) using them.
]

#scos("RuntimePolymorphismExample")

=== Discussion

#skind[Interfaces, classes and methods implemented in the program]

- Interface `Shape`:
  - `void draw()`: Declares a method.

- Class `Circle` (implements `Shape`):
  - Implements the `draw()` method from the `Shape` interface.
  - Method `void calculateArea()`: Declares a specific method for calculating the area of a circle.

- Class `Square` (implements `Shape`):
  - Implements the `draw()` method from the `Shape` interface.
  - Method `void calculateArea()`: Declares a specific method for calculating the area of a square.

- Class `RuntimePolymorphismExample`:
  - `public static void main(String[])`: Contains the main program demonstrating runtime polymorphism by creating objects of subclasses (`Circle` and `Square`) and calling the `draw()` method dynamically.

  
#signature()
