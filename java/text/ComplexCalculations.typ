#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.65em)

#assignment(7)[
  Write a program to add two complex numbers using concept of methods returning objects and methods taking objects as parameters.
]

#scos("ComplexCalculations")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- The `double Math.abs(double)` method returns the absolute value of the given `double` value. 
- `java.util.Scanner` class:
  - `double nextDouble()`: Scan a `double` value from the the input stream.
- `java.util.InputMismatchException` is thrown by the methods of `Scanner` class when encountering invalid input.

#skind[Classes and methods implemented in the program]

- The `Complex` class objects represents a a complex number.
  - Constructor `Complex(double, double)`: Create a complex number object from the provided real and imaginary parts.
  - `public String toString()` method returns the string representation of the complex number.
- The `ComplexOperations` class contains operations that can be performed on `Complex` objects.
  - `static Complex add(Complex a, Complex b) `: Returns the sum of two complex numbers provided.
- The `ComplexCalculations` orchestrates a main program with:
  - `static Complex takeComplexInput()`: Returns the complex number provided by the user.
  - `public static void main(String[])`: Performs addition of two complex numbers.

#signature()
