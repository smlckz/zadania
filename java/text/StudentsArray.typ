#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.6em)

#assignment(11, block: true)[
  Write a program to make a student class with attributes for _roll number_, _name_ and _stream_. Assume that a student studies 3 subjects having full marks of 100 each. Each subject has a _title_ and _theory marks_. From the main class, create an array of such students. Show their specific information, along with average percentage of marks.
]

#scos("StudentsArray", pad: 3em)

=== Discussion

#skind[Classes, and methods used from Java standard library]

- `java.util.Scanner` class:
  - `long nextLong()`: Scan a `long` value from the input stream.
  - `String nextLine()`: Scan a line of text from the input stream.
  - `int nextInt()`: Scan an `int` value from the input stream.

#skind[Classes and methods implemented in the program]

- The `Subject` class represents a subject with a title and theory marks.
  - Constructor `Subject(String, int)`: Create a subject with the given title and theory marks.
  - `static Subject input(Scanner)`: Takes input for subject details from the user.
  - `int getTheoryMarks()`: Returns the theory marks for the subject.
  - `void display()`: Display the marks obtained in the subject.

- The `Student` class represents a student with a roll number, name, stream, and an array of subjects.
  - Constructor `Student(long, String, String, Subject[])`: Create a student with the given details.
  - `static Student input(Scanner)`: Takes input for student details from the user.
  - `void display()`: Display the student details, including roll number, name, stream, subject details, and average percentage.

- The `StudentsArray` class contains the main program to take input for multiple students and display their details:
  - `public static void main(String[])`: Takes the number of students, inputs details for each student, and displays their details including average percentage.

  
#signature()
