#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.5em)

#assignment(25)[
  Write a program in Java to create three threads printing 1 to 10, then implement the program using both inheriting Thread class and implementing Runnable interface.
]

#scos("ThreadsExample")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.lang.Runnable` interface:
  - `void run()`: The method to be implemented by the class that implements this interface.

- `java.lang.Thread` class:
  - `void start()`: Causes this thread to begin execution.
  - `static Thread currentThread()`: Returns a reference to the currently executing thread.
  - `long getId()`: Returns the identifier of this thread.

#skind[Classes and methods implemented in the program]
  
- Class `NumberRunnable` (implements `Runnable`):
  - `public void run()`: Implements the `run` method of the `Runnable` interface, printing numbers.

- Class `NumberThread` (extends `Thread`):
  - `public void run()`: Implements the `run` method of the `Thread` class, printing numbers.

- Main program:
  - Class `ThreadsExample`:
    - `public static void main(String[])`: Demonstrates multi-threading using both `Thread` and `Runnable` approaches, creating and starting threads to print numbers.
  
#signature()
