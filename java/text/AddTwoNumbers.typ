#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")

#assignment(5)[
  Write a program to add two numbers by taking input from command line arguments, the `Scanner` class and the `BufferedReader` class.
]

#scos("AddTwoNumbers")

=== Discussion

#skind[Classes, interfaces and methods used from Java standard library]

- `java.lang.Double` class:
  - `static double parseDouble(String)`: Try to parse the provided string as a `double` value.
- `java.util.Scanner` class:
  - `double nextDouble()`: Scan a `double` value from the the input stream.
- `java.io.BufferedReader` class provides for he efficient reading of characters, arrays, and lines from a character-input stream, buffering characters.
  - Constructor `BufferedReader(Reader)`: Creates a buffering character-input stream for the provided reader stream.
  - `String readLine()`: Returns a line of text from the input stream.
- `java.io.InputStreamReader` class adapts a byte stream into character stream using a specific character set encoding.
  - Constructor `InputStreamReader(InputStream)`: Creates a reader stream for characters from the provided input stream with the default character set encoding.
- `java.io.IOException` class represents the exception to be thrown when an error occurs while performing an I/O operation.
- `java.util.InputMismatchException` is thrown by the methods of `Scanner` class when encountering invalid input.
- `java.lang.NumberFormatExeption` is thrown by `parseDouble` method when the provided string does not constitute a valid `double` value.
  
#skind[Classes and methods implemented in the program]

- The `Number` class objects represents a `double` value.
  - Constructor `Number(double)`: Create a number object for the provided value.
  - `double valueOf()`: Returns the underlying number.
- The `ArithmeticOperations` class has methods implementing operations on `Number`s.
  - `static Number add(Number, Number)`: Adds two `Number`s provided and returns the sum as a `Number`.
- `AddTwoNumbers` class contains the operation of adding when the two numbers have been provided and orchestrates the main program with the `main()` method:
  - `static void process(double, double)`: Performs addition by creating `Number` objects from the two provided `double` values.
  - `public static void main(String[])`: Implements addition of two numbers by taking input:
    - From command line arguments,
    - Utilising `java.util.Scanner`, and
    - Using `java.io.BufferedReader`.
#signature()
