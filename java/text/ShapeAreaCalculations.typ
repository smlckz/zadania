#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.6em)

#assignment(13)[
  Write a program to create a class `Shape` with 4 methods to calculate the areas of triangle, rectangle, square, and circle using method overloading.
]

#scos("ShapeAreaCalculations")

=== Discussion

#skind[Classes and methods implemented in the program]

- The `Shape` class provides static methods for calculating the area of different geometric shapes:
  - `static double area(double a, double b, double c)`: Calculates the area of a triangle using its side lengths.
  - `static int area(int length, int width)`: Calculates the area of a rectangle using its length and width.
  - `static int area(int sideLength)`: Calculates the area of a square using its side length.
  - `static double area(double radius)`: Calculates the area of a circle using its radius.

- The `ShapeAreaCalculations` class contains the main program for calculating the area of geometric shapes based on user input through a menu-driven interface:
  - `static void menu()`: Displays a menu of shape choices.
  - `public static void main(String[])`: Implements a menu-driven program for calculating the area of different shapes (_i.e._ triangle, rectangle, square, or circle) based on user input.

  
#signature()
