#import "/template.typ": *
#show: A => apply(A)
#set raw(lang: "java-new")
#set par(leading: 0.5em)

#assignment(20, block: true)[
  Write a program in which create two interfaces, each with two methods, inherit a new interface from the two, adding a new method, create a class by implementing the new interface and also inheriting a concrete class. In the `main()` method, create an object of the derived class and call these methods.
]

#scos("InterfaceExample")

=== Discussion

#skind[Interfaces, classes and methods implemented in the program]

- Interface `Interface1`:
  - `void method1()`: Declares a method.
  - `void method2()`: Declares another method.

- Interface `Interface2`:
  - `void method3()`: Declares a method.
  - `void method4()`: Declares another method.

- Interface `NewInterface` (extends `Interface1` and `Interface2`):
  - Inherits methods from `Interface1` and `Interface2`.
  - `void newMethod()`: Declares a new method.

- Class `ConcreteClass`:
  - Method `void concreteMethod()`: Declares a method.

- Class `DerivedClass` (extends `ConcreteClass` and implements `NewInterface`):
  - Implements methods from `Interface1`, `Interface2`, and `NewInterface`.
  - Method `void concreteMethod()`: Inherits and overrides a method.

- Class `InterfaceExample`:
  - `public static void main(String[])`: Contains the main program, creating an instance of `DerivedClass` and calling various methods.

  
#signature()
