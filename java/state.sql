-- Assignment status using SQLite

CREATE TABLE `assignments` (
    `name` text, `code` boolean, `output` boolean, `text` boolean, `shown` boolean
);

INSERT INTO `assignments` VALUES 
    ('ArraySearch', 1, 1, 1, 1),
    ('ArraySort', 1, 1, 1, 1),
    ('StringOperations', 1, 1, 1, 1),
    ('MatrixOperations', 1, 1, 1, 1),
    ('AddTwoNumbers', 1, 1, 1, 1),
    ('CylinderCalculations', 1, 1, 1, 1),
    ('ComplexCalculations', 1, 1, 1, 1),
    ('FindNumberInArray', 1, 1, 1, 1),
    ('StaticExecution', 1, 1, 1, 1),
    ('SingletonExample', 1, 1, 1, 1),
    ('StudentsArray', 1, 1, 1, 1),
    ('BankAccountExample', 1, 1, 1, 1),
    ('ShapeAreaCalculations', 1, 1, 1, 0),
    ('AbstractShapeCalculations', 1, 1, 1, 0),
    ('ParentWithTwoChildren', 1, 1, 1, 0),
    ('CuboidCalculations', 1, 1, 1, 0),
    ('EmployeeDisplay', 1, 1, 1, 0),
    ('CircularBaseExample', 1, 1, 1, 0),
    ('InnerClassExample', 1, 1, 1, 0),
    ('InterfaceExample', 1, 1, 1, 0),
    ('RuntimePolymorphismExample', 1, 1, 1, 0),
    ('PackageExample', 1, 1, 1, 0),
    ('PackageInterfaceExample', 1, 1, 1, 0),
    ('CustomExceptionExample', 1, 1, 1, 0),
    ('ThreadsExample', 1, 1, 1, 0);

-- SELECT * FROM `assignments`;
