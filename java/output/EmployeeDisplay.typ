#import "/template.typ": highlight-output
#highlight-output[```
Employee named "Kim Ji-hyung" with ID 87416846.
Scientist named "Daniel Lemire" with ID 14534, 80 publications and 25 years of experience.
Distinguished scientist named "Donald Ervin Knuth" with ID 11, 185 publications, 60 years of experience and awardee of Grace Murray Hopper Award (1971), Turing Award (1974), National Medal of Science (1979), John von Neumann Medal (1995), Harvey Prize (1995), Kyoto Prize (1996), Faraday Medal (2011).
```]

