#import "/template.typ": highlight-output
#highlight-output[```
$ java ArraySort
Menu-driven program of array operations

Enter the array:
Enter the length: 5
Enter the array	elements: 3 -5.2 -4.2 0	6
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform bubble sort
 4. Perform selection sort
 5. Exit

Enter your choice: 3
Before sorting:	The array elements are:	3.0 -5.2 -4.2 0.0 6.0
After sorting: The array elements are: -5.2 -4.2 0.0 3.0 6.0
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform bubble sort
 4. Perform selection sort
 5. Exit

Enter your choice: 2
The array elements are:	-5.2 -4.2 0.0 3.0 6.0
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform bubble sort
 4. Perform selection sort
 5. Exit

Enter your choice: 1
Enter the length: 6
Enter the array	elements: 3.5 -1.3 6.9 11.3 2 -7.8
The array elements are:	3.5 -1.3 6.9 11.3 2.0 -7.8
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform bubble sort
 4. Perform selection sort
 5. Exit

Enter your choice: 4
Before sorting:	The array elements are:	3.5 -1.3 6.9 11.3 2.0 -7.8
After sorting: The array elements are: -7.8 -1.3 2.0 3.5 6.9 11.3
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform bubble sort
 4. Perform selection sort
 5. Exit

Enter your choice: 5
Bye.
```]

