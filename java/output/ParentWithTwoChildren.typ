#import "/template.typ": highlight-output
#highlight-output[```
Parent: ID: 1, Name: John Doe, Address: 123 Main St.
ChildOne: ID: 2, Name: Jane Doe, Address: 456 Elm St.
	Marks: 85
ChildTwo: ID: 3, Name: Mike Smith, Address: 789 Oak St.
	Qualification: B.Tech, Salary: 50000.0
```]

