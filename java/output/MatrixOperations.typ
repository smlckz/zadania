#import "/template.typ": highlight-output
#highlight-output[```
$ java MatrixOperationsCLI 
Menu-driven program for matrix operations
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 1
Enter number of rows and columns: 2 3
Enter the matrix elements: 
1 2 3
4 5 6
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 1
Enter number of rows and columns: 2 3
Enter the matrix elements: 
4 -3 -2
5 7 1
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 3
Enter which two matrices to add (out of 2 matrices): 1 2
The resulting matrix 3:
5.0	-1.0	1.0	
9.0	12.0	7.0	
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 4
Enter which two matrices to subtract (out of 3 matrices): 2 1
The resulting matrix 4:
3.0	-5.0	-5.0	
1.0	2.0	-5.0	
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 1
Enter number of rows and columns: 3 4
Enter the matrix elements: 
1 0 -1 2
3 2 0 1
0 1 -1 2
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 2
Enter which matrix to display (out of 5 matrices): 5
The matrix 5:
1.0	0.0	-1.0	2.0	
3.0	2.0	0.0	1.0	
0.0	1.0	-1.0	2.0	
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 5
Enter which two matrices to multiply (out of 5 matrices): 2 5
The resulting matrix 6:
-5.0	-8.0	-2.0	1.0	
26.0	15.0	-6.0	19.0	
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 5
Enter which two matrices to multiply (out of 6 matrices): 1 2
Error in the operation: Incompatible matrix arguments for multiplication
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 3
Enter which two matrices to add (out of 6 matrices): 2 5
Error in the operation: Incompatible matrix arguments for addition
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 4
Enter which two matrices to subtract (out of 6 matrices): 3 4
The resulting matrix 7:
2.0	4.0	6.0	
8.0	10.0	12.0	
Menu options:
 1. Matrix input
 2. Matrix display
 3. Matrix addition
 4. Matrix subtraction
 5. Matrix multiplication
 6. Exit
Enter your choice: 6
Bye
```]
