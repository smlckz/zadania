#import "/template.typ": highlight-output
#highlight-output[```
$ java ArraySearch 
Menu-driven program of array searching

Enter the array:
Enter the length: 5
Enter the array elements: 5 7 9 3 4
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform linear search
 4. Perform binary search
 5. Exit

Enter your choice: 2
The array elements are: 5.0 7.0 9.0 3.0 4.0
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform linear search
 4. Perform binary search
 5. Exit

Enter your choice: 3
The array elements are: 5.0 7.0 9.0 3.0 4.0
Enter the element to find: 7
The element 7.0 was found in the array at position 2.
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform linear search
 4. Perform binary search
 5. Exit

Enter your choice: 3
The array elements are: 5.0 7.0 9.0 3.0 4.0
Enter the element to find: 8
The element 8.0 was not found in the array.
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform linear search
 4. Perform binary search
 5. Exit

Enter your choice: 4
Array is sorted before binary search.
The array elements are: 3.0 4.0 5.0 7.0 9.0
Enter the element to find: 4
The element 4.0 was found in the array at position 2.
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform linear search
 4. Perform binary search
 5. Exit

Enter your choice: 4
Array is sorted before binary search.
The array elements are: 3.0 4.0 5.0 7.0 9.0
Enter the element to find: 8
The element 8.0 was not found in the array.
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform linear search
 4. Perform binary search
 5. Exit

Enter your choice: 1
Enter the length: 7         
Enter the array elements: 9 -11 7 13 5 0 4  
The array elements are: 9.0 -11.0 7.0 13.0 5.0 0.0 4.0
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform linear search
 4. Perform binary search
 5. Exit

Enter your choice: 2
The array elements are: 9.0 -11.0 7.0 13.0 5.0 0.0 4.0
Menu:
 1. Re-enter array
 2. Display array elements
 3. Perform linear search
 4. Perform binary search
 5. Exit

Enter your choice: 5
Bye.
```]

