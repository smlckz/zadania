#import "/template.typ": highlight-output
#highlight-output[```
$ java AbstractShapeCalculations
Calculate and display shapes

Rectangle
  Enter length: 12
  Enter width: 8
A rectangle with length 12.0 units and width 8.0 units has the area of 96.0 square units.

Circle
  Enter radius: 9
A circle with radius 9.0 units has the area of 254.46900494077323 square units.

Triangle
  Enter three side lengths: 5 12 13
A triangle with side lengths 5.0 units, 12.0 units and 13.0 units has the area of 30.0 square units.
```]

