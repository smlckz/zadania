#import "/template.typ": highlight-output
#highlight-output[```
$ java CustomExceptionExample
Enter an integer: 23
You entered: 23
$ java CustomExceptionExample
Enter an integer: -4
Number cannot be less than zero
$ java CustomExceptionExample
Enter an integer: abc
Input must be an integer.
```]

