#import "/template.typ": highlight-output
#highlight-output[```
$ java StringOperations 
Menu-driven program for string operations
Enter a string: A quick fox jumps over a lazy dog.
Options:
 1. Re-enter a string
 2. Display the string
 3. Count words in the string
 4. Reverse the string
 5. Case-insensitively check whether the string is palindrome or not
 6. Exit

Enter your choice: 2
The string is: A quick brown fox jumps over a lazy dog.
Options:
 1. Re-enter a string
 2. Display the string
 3. Count words in the string
 4. Reverse the string
 5. Case-insensitively check whether the string is palindrome or not
 6. Exit

Enter your choice: 3
The string has 8 words.
Options:
 1. Re-enter a string
 2. Display the string
 3. Count words in the string
 4. Reverse the string
 5. Case-insensitively check whether the string is palindrome or not
 6. Exit

Enter your choice: 4
The given string reversed is: .god yzal eht revo spmuj xof nworb kciuq A
Options:
 1. Re-enter a string
 2. Display the string
 3. Count words in the string
 4. Reverse the string
 5. Case-insensitively check whether the string is palindrome or not
 6. Exit

Enter your choice: 5
The given string isn't case-insensitively palindrome.
Options:
 1. Re-enter a string
 2. Display the string
 3. Count words in the string
 4. Reverse the string
 5. Case-insensitively check whether the string is palindrome or not
 6. Exit

Enter your choice: 1
Enter a string: Aa bB C bB Aa
Options:
 1. Re-enter a string
 2. Display the string
 3. Count words in the string
 4. Reverse the string
 5. Case-insensitively check whether the string is palindrome or not
 6. Exit

Enter your choice: 5
The given string is case-insensitively palindrome.
Options:
 1. Re-enter a string
 2. Display the string
 3. Count words in the string
 4. Reverse the string
 5. Case-insensitively check whether the string is palindrome or not
 6. Exit

Enter your choice: 6
Bye.
```]
