#import "/template.typ": highlight-output
#highlight-output[```
$ java StaticExecution 
Executing static block in StaticExecution
Executing static main method in StaticExecution
Executing static block in StaticExample
Executing StaticExample constructor for the first time
The number of objects of StaticExample created: 7
```]

