#import "/template.typ": highlight-output
#highlight-output[```
$ java CircularBaseExample
Enter radius of Cone: 5
Enter height of Cone: 8
Enter radius of Cylinder: 7
Enter height of Cylinder: 6

Cone base area: 78.53981633974483
Cone total area: 204.20352248333654
Cone volume: 209.43951023931953

Cylinder base area: 153.93804002589985
Cylinder total area: 571.7698629533423
Cylinder volume: 923.6282401553991
```]

