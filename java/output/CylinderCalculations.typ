#import "/template.typ": highlight-output
==== Taking input from command line arguments
#highlight-output[```
$ java CylinderCalculationsCLI
Usage: CylinderCalculationsCLI radius height
$ java CylinderCalculationsCLI 2 6
A cylinder with radius 2.0 units and height 6.0 units, has volume of 75.39822368615503 cubic units and surface area of 603.1857894892403 square units.

```]
==== Taking input from keyboard using `java.util.Scanner`
#highlight-output[```
$ java CylinderCalculationsScan
Enter radius: 5
Enter height: 13
A cylinder with radius 5.0 units and height 13.0 units, has volume of 1021.0176124166828 cubic units and surface area of 7351.326809400116 square units.
```]
