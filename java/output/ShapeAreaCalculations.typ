#import "/template.typ": highlight-output
#highlight-output[```
$ java ShapeAreaCalculations
Choose the shape: 
1. Triangle
2. Rectangle
3. Square
4. Circle
Enter your choice: 1
Enter sides: 6 8 10
Area of a triangle: 24.0
$ java ShapeAreaCalculations
Choose the shape: 
1. Triangle
2. Rectangle
3. Square
4. Circle
Enter your choice: 2
Enter length and width: 23 14
Area of a rectangle: 322
$ java ShapeAreaCalculations
Choose the shape: 
1. Triangle
2. Rectangle
3. Square
4. Circle
Enter your choice: 3
Enter side length: 7
Area of a square: 49
$ java ShapeAreaCalculations
Choose the shape: 
1. Triangle
2. Rectangle
3. Square
4. Circle
Enter your choice: 4
Enter radius: 5
Area of a square: 78.53981633974483
```]

