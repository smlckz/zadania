#import "/template.typ": highlight-output
#v(0.8em)
#columns(2)[
#highlight-output[```
$ java ThreadsExample
Subclass Thread 13 - Tick 1
Subclass Thread 13 - Tick 2
Subclass Thread 13 - Tick 3
Subclass Thread 13 - Tick 4
Subclass Thread 13 - Tick 5
```]
#highlight-output[```
Subclass Thread 13 - Tick 6
Subclass Thread 13 - Tick 7
Subclass Thread 13 - Tick 8
Subclass Thread 13 - Tick 9
Subclass Thread 13 - Tick 10
Runnable Thread 18 - Tick 1
```]
#highlight-output[```
Subclass Thread 14 - Tick 1
Runnable Thread 18 - Tick 2
Runnable Thread 17 - Tick 1
Subclass Thread 14 - Tick 2
Runnable Thread 17 - Tick 2
Runnable Thread 16 - Tick 1
Subclass Thread 15 - Tick 1
Runnable Thread 16 - Tick 2
Runnable Thread 17 - Tick 3
Runnable Thread 17 - Tick 4
Runnable Thread 17 - Tick 5
Runnable Thread 17 - Tick 6
Runnable Thread 17 - Tick 7
Runnable Thread 17 - Tick 8
Runnable Thread 17 - Tick 9
Runnable Thread 17 - Tick 10
Subclass Thread 14 - Tick 3
Runnable Thread 18 - Tick 3
Runnable Thread 16 - Tick 3
Subclass Thread 14 - Tick 4
Subclass Thread 14 - Tick 5
Subclass Thread 14 - Tick 6
Subclass Thread 14 - Tick 7
Subclass Thread 14 - Tick 8
Subclass Thread 14 - Tick 9
```]
#colbreak()
#highlight-output[```
Subclass Thread 14 - Tick 10
Runnable Thread 16 - Tick 4
Runnable Thread 16 - Tick 5
Runnable Thread 16 - Tick 6
Subclass Thread 15 - Tick 2
Runnable Thread 16 - Tick 7
Runnable Thread 18 - Tick 4
Runnable Thread 18 - Tick 5
Runnable Thread 18 - Tick 6
Runnable Thread 18 - Tick 7
Runnable Thread 18 - Tick 8
Runnable Thread 16 - Tick 8
Subclass Thread 15 - Tick 3
Runnable Thread 16 - Tick 9
Subclass Thread 15 - Tick 4
Runnable Thread 18 - Tick 9
Subclass Thread 15 - Tick 5
Runnable Thread 16 - Tick 10
Subclass Thread 15 - Tick 6
Subclass Thread 15 - Tick 7
Subclass Thread 15 - Tick 8
Subclass Thread 15 - Tick 9
Subclass Thread 15 - Tick 10
Runnable Thread 18 - Tick 10
```]]
