#import "/template.typ": highlight-output
#highlight-output[```
$ java StudentsArray
Enter the number of students: 
2
Enter the details of the students:
For student 1: Enter the details of the student:
Enter Roll no.: 1
Enter name: Lee Haseol
Enter stream: Science
For subject 1: Enter details of subject:
Enter title: Physics
Enter theory marks: 94
For subject 2: Enter details of subject:
Enter title: Chemistry
Enter theory marks: 96
For subject 3: Enter details of subject:
Enter title: Mathematics
Enter theory marks: 98
For student 2: Enter the details of the student:
Enter Roll no.: 43
Enter name: Lin Feng
Enter stream: Arts
For subject 1: Enter details of subject:
Enter title: History
Enter theory marks: 47
For subject 2: Enter details of subject:
Enter title: Caligraphy
Enter theory marks: 56
For subject 3: Enter details of subject:
Enter title: Philosophy
Enter theory marks: 72
Displaying the details of the students:

For student 1: 
Student details:
  Roll No.: 1
  Name: Lee Haseol
  Stream: Science
  Marks obtained in Physics: 94
  Marks obtained in Chemistry: 96
  Marks obtained in Mathematics: 98
  Average percentage: 96.00%

For student 2: 
Student details:
  Roll No.: 43
  Name: Lin Feng
  Stream: Arts
  Marks obtained in History: 47
  Marks obtained in Caligraphy: 56
  Marks obtained in Philosophy: 72
  Average percentage: 58.33%
```]

