#import "/template.typ": highlight-output
#highlight-output[```
$ java BankAccountExample
Menu-driven program for operating bank accounts:
Menu:
 1. Create bank account
 2. Display bank account details
 3. Deposit money
 4. Withdraw money
 5. Close bank account
 6. Exit

Enter your choice: 1
Enter details of bank account:
  Name: Lee Heoksoo
  Address: Sindang-dong, Seoul
  Account No.: 120045387091
  Balance: 71623500
Menu:
 1. Create bank account
 2. Display bank account details
 3. Deposit money
 4. Withdraw money
 5. Close bank account
 6. Exit

Enter your choice: 2
Enter account number: 120045387091
Customer details: 
  Name: Lee Heoksoo
  Address: Sindang-dong, Seoul
  Account Number: 120045387091
  Balance: 71623500.00

Menu:
 1. Create bank account
 2. Display bank account details
 3. Deposit money
 4. Withdraw money
 5. Close bank account
 6. Exit

Enter your choice: 1
Enter details of bank account:
  Name: Axel Schauer
  Address: Eidelstedt, Hamburg
  Account No.: 582978
  Balance: 36400
Menu:
 1. Create bank account
 2. Display bank account details
 3. Deposit money
 4. Withdraw money
 5. Close bank account
 6. Exit

Enter your choice: 4
Enter account number: 42000
Could not find a bank account with the given account number.
Try again.

Menu:
 1. Create bank account
 2. Display bank account details
 3. Deposit money
 4. Withdraw money
 5. Close bank account
 6. Exit

Enter your choice: 4
Enter account number: 4200
Could not find a bank account with the given account number.
Try again.

Menu:
 1. Create bank account
 2. Display bank account details
 3. Deposit money
 4. Withdraw money
 5. Close bank account
 6. Exit

Enter your choice: 3
Enter account number: 12000
Could not find a bank account with the given account number.
Try again.

Menu:
 1. Create bank account
 2. Display bank account details
 3. Deposit money
 4. Withdraw money
 5. Close bank account
 6. Exit

Enter your choice: 5
Enter account number: 582978
Are you sure about closing your account (y/n): y
Account closing successful.
Menu:
 1. Create bank account
 2. Display bank account details
 3. Deposit money
 4. Withdraw money
 5. Close bank account
 6. Exit

Enter your choice: 2
Enter account number: 582978
Could not find a bank account with the given account number.
Try again.

Menu:
 1. Create bank account
 2. Display bank account details
 3. Deposit money
 4. Withdraw money
 5. Close bank account
 6. Exit

Enter your choice: 6
Bye.
```]

