#import "/template.typ": highlight-output
#highlight-output[```
$ java AddTwoNumbersCLI 5 7
Taking input from CLI arguments:
5.0 + 7.0 = 12.0
Taking input using java.util.Scanner:
Enter first number: 12 
Enter second number: 7.3
12.0 + 7.3 = 19.3
Taking input using java.io.BufferedReader:
Enter first number: 11.1
Enter second number: 9.6
11.1 + 9.6 = 20.7
```]
