#import "/template.typ": highlight-output
#highlight-output[```
$ java FindNumberInArray
Enter length: 5
Enter array elements: 4 7 3 2 9
Given: An array of numbers with 5 elements: 4.0 7.0 3.0 2.0 9.0
Enter element to find in array: 7
The number 7.0 was found in the array at position 2.
$ java FindNumberInArray
Enter length: 4
Enter array elements: 5 9 0 3
Given: An array of numbers with 4 elements: 5.0 9.0 0.0 3.0
Enter element to find in array: 4
Could not find the number 4.0 in the array.
```]
