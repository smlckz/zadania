import java.util.Scanner;
import java.util.InputMismatchException;

class Cone {
    double radius, height;
    Cone(double r, double h) {
        radius = r;
        height = h;
    }
    double volume() {
        return Math.PI * radius * radius * height / 3.0;
    }
    double surfaceArea() {
        return Math.PI * radius * (radius + Math.sqrt(height * height + radius * radius));
    }
    void display() {
        System.out.println("A cone with radius " + radius + " units and height " + height + " units has surface area " + surfaceArea() + " square units and volume " + volume() + " cubic units.");
    }
}

class ConeCalculationsCLI {
    public static void main(String args[]) {
        if (args.length != 2) {
            System.err.println("Usage: ConeCalculationsCLI radius height");
            System.exit(1);
        }
        try {
            var radius = Double.parseDouble(args[0]);
            var height = Double.parseDouble(args[1]);
            var c = new Cone(radius, height);
            c.display();
        } catch (NumberFormatException e) {
            System.err.println("Invalid number given as input");
            System.exit(1);
        }
    }
}

class ConeCalculationsScan {
    public static void main(String args[]) {
        try {
            var sc = new Scanner(System.in);
            System.out.print("Enter radius: ");
            var radius = sc.nextDouble();
            System.out.print("Enter height: ");
            var height = sc.nextDouble();
            var c = new Cone(radius, height);
            c.display();
        } catch (InputMismatchException e) {
            System.err.println("Invalid number given as input");
            System.exit(1);
        }
    }
}

