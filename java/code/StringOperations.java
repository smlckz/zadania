import java.util.Scanner;

class MyString {
    String str;

    MyString(String s) { str = s; }

    String valueOf() { return str; }

    int countWords() {
        int count = 0, size = str.length();
        enum state { WORD, SPACE };
        state st = state.WORD;
        for (int i = 0; i < size; i++) {
            char c = str.charAt(i);
            if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {
                st = state.SPACE;
            } else if (st != state.WORD) {
                st = state.WORD;
                count++;
            }
        }
        return count;
    }

    MyString reverse() {
        char[] arr = str.toCharArray();
        int end = arr.length - 1;
        for (int i = 0; i < arr.length / 2; i++, end--) {
            char tmp = arr[i];
            arr[i] = arr[end];
            arr[end] = tmp;
        }
        return new MyString(String.copyValueOf(arr));
    }
    
    MyString toLowerCase() {
        char[] arr = str.toCharArray();
        char[] narr = new char[arr.length];
        for (int i = 0; i < arr.length; i++) {
            char c = arr[i];
            if ('A' <= c && c <= 'Z') c += 'a' - 'A';
            narr[i] = c;
        }
        return new MyString(String.copyValueOf(narr));
    }
    
    boolean equals(MyString ms) {
        char[] arr1 = str.toCharArray();
        char[] arr2 = ms.valueOf().toCharArray();
        if (arr1.length != arr2.length) return false;
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) return false;
        }
        return true;
    }

    boolean isCaseInsensitivePalindrome() {
        return str.toLowerCase().equals(reverse().toLowerCase().valueOf());
    }
}

class StringOperations {
    static void menu() {
        System.out.println(
            "Options:\n" +
            " 1. Re-enter a string\n" +
            " 2. Display the string\n" +
            " 3. Count words in the string\n" +
            " 4. Reverse the string\n" +
            " 5. Case-insensitively check whether the string is palindrome or not\n" +
            " 6. Exit\n");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Menu-driven program for string operations");
        System.out.print("Enter a string: ");
        MyString ms = new MyString(sc.nextLine());
        while (true) {
            menu();
            System.out.print("Enter your choice: ");
            int choice = sc.nextInt();
            sc.skip("\n");
            switch (choice) {
            case 1:
                System.out.print("Enter a string: ");
                ms = new MyString(sc.nextLine());
                break;
            case 2:
                System.out.println("The string is: " + ms.valueOf());
                break;
            case 3:
                int count = ms.countWords();
                System.out.println("The string has " + count + " words.");
                break;
            case 4:
                System.out.println("The given string reversed is: " + ms.reverse().valueOf());
                break;
            case 5:
                System.out.println("The given string is" +
                        (ms.isCaseInsensitivePalindrome() ? "" : "n't") +
                        " case-insensitively palindrome.");
                break;
            case 6:
                System.out.println("Bye.");
                return;
            default:
                System.out.println("Invalid choice, try again.");
            }
        }
    }
}
