import java.util.Random;

class StaticExample {
    private static int objectCount = 0;
    static int getObjectCount() { return objectCount; }
    static {
        System.out.println("Executing static block in StaticExample");
    }
    StaticExample() {
        if (objectCount == 0)
            System.out.println("Executing StaticExample constructor for the first time");
        objectCount++;
    }
}

class StaticExecution {
    static {
        System.out.println("Executing static block in StaticExecution");
    }
    public static void main(String args[]) {
        System.out.println("Executing static main method in StaticExecution");
        int c = new Random().nextInt(10) + 1;
        for (int i = 0; i < c; i++) {
            var ex = new StaticExample();
        }
        System.out.println("The number of objects of StaticExample created: " +
            StaticExample.getObjectCount());
    }
}

