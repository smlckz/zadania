class NumberRunnable implements Runnable {
    public void run() {
        for (int i = 1; i <= 10; i++) {
            System.out.println("Runnable Thread " + Thread.currentThread().getId() + " - Tick " + i);
        }
    }
}

class NumberThread extends Thread {
    public void run() {
        for (int i = 1; i <= 10; i++) {
            System.out.println("Subclass Thread " + Thread.currentThread().getId() + " - Tick " + i);
        }
    }
}

public class ThreadsExample {
    public static void main(String[] args) {
        Thread[] threads = new Thread[6];

        // Creating three threads using NumberThread class (inheriting Thread)
        for (int i = 0; i < 3; i++) {
            threads[i] = new NumberThread();
        }

        // Creating three threads using NumberRunnable class (implementing Runnable)
        for (int i = 3; i < 6; i++) {
            threads[i] = new Thread(new NumberRunnable());
        }

        // Starting all threads
        for (Thread thread : threads) {
            thread.start();
        }
    }
}
