class Employee {
    int id;
    String name;
    Employee(int eid, String ename) {
        id = eid;
        name = ename;
    }
    public String toString() {
        return "Employee named \"" + name + "\" with ID " + id + ".";
    }
}

class Scientist extends Employee {
    int number_of_publications, experience;
    Scientist(int sid, String sname, int nopubs, int yexp) {
        super(sid, sname);
        number_of_publications = nopubs;
        experience = yexp;
    }
    public String toString() {
        return "Scientist named \"" + name + "\" with ID " + id + ", " +
            number_of_publications + " publications and " + experience +
            " years of experience" + ".";
    }
}

class DScientist extends Scientist {
    String[] award;
    DScientist(int sid, String sname, int nopubs, int yexp,
               String[] dsaward) {
        super(sid, sname, nopubs, yexp);
        award = dsaward;
    }
    public String toString() {
        return "Distinguished scientist named \"" + name + "\" with ID " + id +
            ", " + number_of_publications + " publications, " + experience +
            " years of experience and awardee of " + String.join(", ", award) + ".";
    }
}

class EmployeeDisplay {
    public static void main(String args[]) {
        Employee e;
        e = new Employee(87416846, "Kim Ji-hyung");
        System.out.println(e);
        e = new Scientist(14534, "Daniel Lemire", 80, 25);
        System.out.println(e);
        e = new DScientist(11, "Donald Ervin Knuth", 185, 60,
            new String[] { "Grace Murray Hopper Award (1971)", "Turing Award (1974)",
                "National Medal of Science (1979)", "John von Neumann Medal (1995)",
                "Harvey Prize (1995)", "Kyoto Prize (1996)", "Faraday Medal (2011)"});
        System.out.println(e);
    }
}

