package pTwo;
import pOne.ClassOne;

public class ClassTwo extends ClassOne {
    public void methodTwo() {
        System.out.println("MethodTwo in ClassTwo");

        // Accessing variable and method from ClassOne
        System.out.println("Variable from ClassOne: " + variableOne);
        methodOne();
    }
}
