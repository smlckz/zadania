import java.util.InputMismatchException;
import java.util.Scanner;

interface ThreeDimensionalShape {
    double calArea();
    double calVolume();
}

class CircularBase {
    double radius;

    CircularBase(double radius) {
        this.radius = radius;
    }

    double getArea() {
        return Math.PI * radius * radius;
    }
}

class Cone extends CircularBase implements ThreeDimensionalShape {
    double height;

    Cone(double radius, double height) {
        super(radius);
        this.height = height;
    }

    @Override
    public double calArea() {
	return getArea() + Math.PI * radius * height;
    }

    @Override
    public double calVolume() {
        return (1.0 / 3.0) * getArea() * height;
    }
}

class Cylinder extends CircularBase implements ThreeDimensionalShape {
    double height;

    Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    @Override
    public double calArea() {
        return 2 * getArea() + 2 * Math.PI * radius * height;
    }

    @Override
    public double calVolume() {
        return getArea() * height;
    }
}

public class CircularBaseExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("Enter radius of Cone: ");
            double coneRadius = scanner.nextDouble();
            System.out.print("Enter height of Cone: ");
            double coneHeight = scanner.nextDouble();
            System.out.print("Enter radius of Cylinder: ");
            double cylinderRadius = scanner.nextDouble();
            System.out.print("Enter height of Cylinder: ");
            double cylinderHeight = scanner.nextDouble();
            Cone cone = new Cone(coneRadius, coneHeight);
            Cylinder cylinder = new Cylinder(cylinderRadius, cylinderHeight);

            System.out.println("\nCone base area: " + cone.getArea());
            System.out.println("Cone total area: " + cone.calArea());
            System.out.println("Cone volume: " + cone.calVolume());

            System.out.println("\nCylinder base area: " + cylinder.getArea());
            System.out.println("Cylinder total area: " + cylinder.calArea());
            System.out.println("Cylinder volume: " + cylinder.calVolume());
        } catch (InputMismatchException e) {
            System.err.println("Invalid input. Please enter numerical values only.");
            System.exit(1);
        }
    }
}
