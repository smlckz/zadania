class Parent {
    int id;
    String name;
    String address;

    Parent(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    void display() {
        String className = this.getClass().getSimpleName();
        System.out.println(className + ": ID: " + id + ", Name: " + name + ", Address: " + address);
    }
}

class ChildOne extends Parent {
    int marks;

    ChildOne(int id, String name, String address, int marks) {
        super(id, name, address);
        this.marks = marks;
    }

    @Override
    void display() {
        super.display();
        System.out.println("\tMarks: " + marks);
    }
}

class ChildTwo extends Parent {
    String qualification;
    double salary;

    ChildTwo(int id, String name, String address, String qualification, double salary) {
        super(id, name, address);
        this.qualification = qualification;
        this.salary = salary;
    }

    @Override
    void display() {
        super.display();
        System.out.println("\tQualification: " + qualification + ", Salary: " + salary);
    }
}

class ParentWithTwoChildren {
    public static void main(String[] args) {
        Parent parent1 = new Parent(1, "John Doe", "123 Main St.");
        ChildOne child1 = new ChildOne(2, "Jane Doe", "456 Elm St.", 85);
        ChildTwo child2 = new ChildTwo(3, "Mike Smith", "789 Oak St.", "B.Tech", 50000);

        parent1.display();
        child1.display();
        child2.display();
    }
}
