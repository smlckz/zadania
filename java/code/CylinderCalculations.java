import java.util.Scanner;
import java.util.InputMismatchException;

class Cylinder {
    double radius, height;
    Cylinder(double r, double h) {
        radius = r;
        height = h;
    }
    double volume() {
        return Math.PI * radius * radius * height;
    }
    double surfaceArea() {
        return 2 * Math.PI * radius * height * (radius + height);
    }
    void display() {
        System.out.println("A cylinder with radius " + radius + " units and height " + height + " units, has volume of " + volume() + " cubic units and surface area of " + surfaceArea() + " square units.");
    }
}

class CylinderCalculationsCLI {
    public static void main(String args[]) {
        if (args.length != 2) {
            System.err.println("Usage: CylinderCalculationsCLI radius height");
            System.exit(1);
        }
        try {
            var radius = Double.parseDouble(args[0]);
            var height = Double.parseDouble(args[1]);
            var cy = new Cylinder(radius, height);
            cy.display();
        } catch (NumberFormatException e) {
            System.err.println("Invalid numbers");
        }
    }
}

class CylinderCalculationsScan {
    public static void main(String args[]) {
        try {
            var sc = new Scanner(System.in);
            System.out.print("Enter radius: ");
            var radius = sc.nextDouble();
            System.out.print("Enter height: ");
            var height = sc.nextDouble();
            var cy = new Cylinder(radius, height);
            cy.display();
        } catch (InputMismatchException e) {
            System.err.println("Invalid numbers");
        }
    }
}
