class Rectangle {
    int length;
    int breadth;

    Rectangle(int length, int breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    int area() {
        return length * breadth;
    }
}

class Cuboid extends Rectangle {
    int height;

    Cuboid(int length, int breadth, int height) {
        super(length, breadth);
        this.height = height;
    }

    int surfaceArea() {
        return 2 * (area() + (length + breadth) * height);
    }

    int volume() {
        return area() * height;
    }
}

public class CuboidCalculations {
    public static void main(String[] args) {
        Cuboid cuboid = new Cuboid(5, 4, 3);

        System.out.println("Surface area of the Cuboid: " + cuboid.surfaceArea());
        System.out.println("Volume of the Cuboid: " + cuboid.volume());
    }
}
