import java.util.ArrayList;
import java.util.Scanner;

class BankAccount {
    String name, address;
    long accountNo;
    double balance;
    BankAccount(String name, String address, long accountNo, double balance) {
        this.name = name;
        this.address = address;
        this.accountNo = accountNo;
        this.balance = balance;
    }
    static BankAccount takeInput(Scanner sc) {
        System.out.println("Enter details of bank account:");
        System.out.print("  Name: ");
        String name = sc.nextLine();
        System.out.print("  Address: ");
        String address = sc.nextLine();
        System.out.print("  Account No.: ");
        long accountNo = sc.nextLong();
        System.out.print("  Balance: ");
        double balance = sc.nextDouble();
        sc.skip("\n");
        return new BankAccount(name, address, accountNo, balance);
    }
    long getAccountNo() { return accountNo; }
    double getBalance() { return balance; }
    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof BankAccount && accountNo == ((BankAccount)o).getAccountNo();
    }
    void deposit(double amount) throws IllegalArgumentException {
        if (amount < 0.0)
            throw new IllegalArgumentException(
                "Can not deposit negative amount of money. Perform withdrawal instead.");
        balance += amount;
    }
    static String formatBalance(double value) { return String.format("%.2f", value); }
    void withdraw(double amount) throws Exception {
        if (amount < 0.0)
            throw new IllegalArgumentException(
                "Can not withdraw negative amount of money. Perform deposit instead.");
        else if (amount > balance)
            throw new Exception(
                "Available balance: " + formatBalance(balance) +
                " but tried to withdraw: " + formatBalance(amount) +
                " with the shortfall of " + formatBalance(amount - balance));
        balance -= amount;
    }
    void display() {
        System.out.println(
            "Customer details: \n" +
            "  Name: " + name + "\n" +
            "  Address: " + address + "\n" +
            "  Account Number: " + accountNo + "\n" +
            "  Balance: " + formatBalance(balance) + "\n");
    }
    static BankAccount lookup(ArrayList<BankAccount> acs, long accountNo) {
        for (var ac : acs) {
            if (ac.getAccountNo() == accountNo) return ac;
        }
        return null;
    }
}

class BankAccountExample {
    static void menu() {
        System.out.println(
            "Menu:\n" +
            " 1. Create bank account\n" +
            " 2. Display bank account details\n" +
            " 3. Deposit money\n" +
            " 4. Withdraw money\n" +
            " 5. Close bank account\n" +
            " 6. Exit\n");
    }
    static BankAccount findAccount(Scanner sc, ArrayList<BankAccount> accounts) {
        System.out.print("Enter account number: ");
        long accountNo = sc.nextLong();
        sc.skip("\n");
        var ac = BankAccount.lookup(accounts, accountNo);
        if (ac == null) {
            System.out.println("Could not find a bank account with the given account number.");
            System.out.println("Try again.\n");
            return null;
        }
        return ac;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Menu-driven program for operating bank accounts:");
        var accounts = new ArrayList<BankAccount>();
        while (true) {
            menu();
            System.out.print("Enter your choice: ");
            int choice = sc.nextInt();
            sc.skip("\n");
            double amount;
            BankAccount ac;
            switch (choice) {
            case 1:
                ac = BankAccount.takeInput(sc);
                if (BankAccount.lookup(accounts, ac.getAccountNo()) != null) {
                    System.out.println("An account already exists with the same account number.");
                    System.out.println("Can not create account, try again.");
                    continue;
                }
                accounts.add(ac);
                break;
            case 2:
                ac = findAccount(sc, accounts);
                if (ac == null) continue;
                ac.display();
                break;
            case 3:
                ac = findAccount(sc, accounts);
                if (ac == null) continue;
                System.out.print("Enter the amount to deposit: ");
                amount = sc.nextDouble();
                try {
                    double balanceBefore = ac.getBalance();
                    ac.deposit(amount);
                    double balanceAfter = ac.getBalance();
                    System.out.println("Operation successful.");
                    System.out.println(
                        "Balance:\n" +
                        "  before: " + BankAccount.formatBalance(balanceBefore) + "\n" +
                        "  after: " + BankAccount.formatBalance(balanceAfter) + "\n");
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }
                break;
            case 4:
                ac = findAccount(sc, accounts);
                if (ac == null) continue;
                System.out.print("Enter the amount to withdraw: ");
                amount = sc.nextDouble();
                try {
                    double balanceBefore = ac.getBalance();
                    ac.withdraw(amount);
                    double balanceAfter = ac.getBalance();
                    System.out.println("Operation successful.");
                    System.out.println(
                        "Balance:\n" +
                        "  before: " + BankAccount.formatBalance(balanceBefore) + "\n" +
                        "  after: " + BankAccount.formatBalance(balanceAfter) + "\n");
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }
                break;
            case 5:
                ac = findAccount(sc, accounts);
                if (ac == null) continue;
                System.out.print("Are you sure about closing your account (y/n): ");
                var response = sc.nextLine();
                if (!response.equalsIgnoreCase("y")) {
                    System.out.println("Account closing aborted.");
                    continue;
                }
                accounts.remove(ac);
                System.out.println("Account closing successful.");
                break;
            case 6:
                System.out.println("Bye.");
                return;
            default:
                System.out.println("Invalid choice, try again");
            }
        }
    }
}
