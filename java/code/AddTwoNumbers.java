import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.InputMismatchException;

class Number {
    private double value;
    Number(double n) {
        value = n;
    }
    double valueOf() {
        return value;
    }
}

class ArithmeticOperations {
    static Number add(Number a, Number b) {
        return new Number(a.valueOf() + b.valueOf());
    }
}

class AddTwoNumbers {
    static void process(double a, double b) {
        Number n1 = new Number(a), n2 = new Number(b);
        System.out.println(n1.valueOf() + " + " + n2.valueOf() + " = " +
            ArithmeticOperations.add(n1, n2).valueOf());
    }
    public static void main(String args[]) {
        try {
            if (args.length != 2) {
                System.err.println("Usage: AddTwoNumbersCLI first-number second-number");
            } else {
                System.out.println("Taking input from CLI arguments:");
                var v1 = Double.parseDouble(args[0]);
                var v2 = Double.parseDouble(args[1]);
                process(v1, v2);
            }
            double v1, v2;
            System.out.println("Taking input using java.util.Scanner:");
            var sc = new Scanner(System.in);
            System.out.print("Enter first number: ");
            v1 = sc.nextDouble();
            System.out.print("Enter second number: ");
            v2 = sc.nextDouble();
            process(v1, v2);
            System.out.println("Taking input using java.io.BufferedReader:");
            var r = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter first number: ");
            v1 = Double.parseDouble(r.readLine());
            System.out.print("Enter second number: ");
            v2 = Double.parseDouble(r.readLine());
            process(v1, v2);
        } catch (IOException e) {
            System.err.println("I/O error occured while reading input.");
        } catch (InputMismatchException e) {
            System.err.println("Invalid numbers");
        } catch (NumberFormatException e) {
            System.err.println("Invalid numbers");
        }
    }
}
