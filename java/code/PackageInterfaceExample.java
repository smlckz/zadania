import pkgTwo.MyClass;
import pkgOne.MyInterface;

public class PackageInterfaceExample {
    public static void main(String[] args) {
        MyInterface obj = new MyClass();

        obj.methodOne();
        obj.methodTwo();
        obj.methodThree();
    }
}
