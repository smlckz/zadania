import java.util.ArrayList;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.function.BinaryOperator;

class Matrix {
    private int rows, cols;
    private float mat[][];
    Matrix(int r, int c) {
        rows = r;
        cols = c;
        mat = new float[rows][cols];
    }
    int rows() { return rows; }
    int cols() { return cols; }
    float get(int i, int j) { return mat[i][j]; }
    void set(int i, int j, float v) { mat[i][j] = v; }
    void input(Scanner sc) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                mat[i][j] = sc.nextFloat();
            }
        }	
    }
    void display() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.format(mat[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

class MatrixOperations {
    static Matrix add(Matrix x, Matrix y) throws IllegalArgumentException {
        int xr = x.rows(), xc = x.cols(), yr = y.rows(), yc = y.cols();
        if (!((xr == yr) && (xc == yc))) {
            throw new IllegalArgumentException("Incompatible matrix arguments for addition");
        }
        Matrix s = new Matrix(xr, yc);
        for (int i = 0; i < xr; i++) {
            for (int j = 0; j < yc; j++) {
                var v = x.get(i, j) + y.get(i, j);
                s.set(i, j, v);
            }
        }
        return s;
    }
    static Matrix subtract(Matrix x, Matrix y) throws IllegalArgumentException {
        int xr = x.rows(), xc = x.cols(), yr = y.rows(), yc = y.cols();
        if (!((xr == yr) && (xc == yc))) {
            throw new IllegalArgumentException("Incompatible matrix arguments for subtraction");
        }
        Matrix d = new Matrix(xr, yc);
        for (int i = 0; i < xr; i++) {
            for (int j = 0; j < yc; j++) {
                var v = x.get(i, j) - y.get(i, j);
                d.set(i, j, v);
            }
        }
        return d;
    }
    static Matrix multiply(Matrix x, Matrix y) throws IllegalArgumentException {
        int xr = x.rows(), xc = x.cols(), yr = y.rows(), yc = y.cols();
        if (xc != yr) {
            throw new IllegalArgumentException("Incompatible matrix arguments for multiplication");
        }
        Matrix p = new Matrix(xr, yc);
        for (int i = 0; i < xr; i++) {
            for (int j = 0; j < yc; j++) {
                float v = 0;
                for (int k = 0; k < xc; k++) {
                    v += x.get(i, k) * y.get(k, j);
                }
                p.set(i, j, v);
            }
        }
        return p;
    }
}

class MatrixOperationsCLI {
    static void menu() {
        System.out.println(
            "Menu options:\n" +
            " 1. Matrix input\n" +
            " 2. Matrix display\n" +
            " 3. Matrix addition\n" +
            " 4. Matrix subtraction\n" +
            " 5. Matrix multiplication\n" +
            " 6. Exit");
    }
    static Matrix takeMatrix(Scanner sc) {
        System.out.print("Enter number of rows and columns: ");
        var rows = sc.nextInt();
        var cols = sc.nextInt();
        var m = new Matrix(rows, cols);
        System.out.println("Enter the matrix elements: ");
        m.input(sc);
        return m;
    }
    static void displayMatrix(Scanner sc, ArrayList<Matrix> ms) {
        var size = ms.size();
        System.out.print("Enter which matrix to display " +
                         "(out of " + size + " matrices): ");
        var index = sc.nextInt() - 1;
        if (index < 0 || index > size) {
            System.err.println("Invalid index of matrix");
            return;
        }
        System.out.println("The matrix " + (index + 1) + ":");
        ms.get(index).display();
    }
    static void operate(Scanner sc, ArrayList<Matrix> ms, String opName, BinaryOperator<Matrix> op) {
        var size = ms.size();
        System.out.print("Enter which two matrices to " + opName +
                         " (out of " + size + " matrices): ");
        int xi = sc.nextInt() - 1, yi = sc.nextInt() - 1;
        if (xi < 0 || xi > size || yi < 0 || yi > size) {
            System.err.println("Invalid index of matrix");
            return;
        }
        try {
            var m = op.apply(ms.get(xi), ms.get(yi));
            ms.add(m);
            System.out.println("The resulting matrix " + (size + 1) + ":");
            m.display();
        } catch (IllegalArgumentException e) {
            System.out.println("Error in the operation: " + e.getMessage());
        }
    }
    public static void main(String args[]) {
        var sc = new Scanner(System.in);
        var ms = new ArrayList<Matrix>(); 
        System.out.println("Menu-driven program for matrix operations");
        while (true) {
            try {
                menu();
                System.out.print("Enter your choice: ");
                var choice = sc.nextInt();
                switch (choice) {
                case 1:
                    ms.add(takeMatrix(sc));
                    break;
                case 2:
                    displayMatrix(sc, ms);
                    break;
                case 3:
                    operate(sc, ms, "add", MatrixOperations::add);
                    break;
                case 4:
                    operate(sc, ms, "subtract", MatrixOperations::subtract);
                    break;
                case 5:
                    operate(sc, ms, "multiply", MatrixOperations::multiply);
                    break;
                case 6:
                    System.out.println("Bye");
                    return;
                default:
                    System.err.println("Invalid choice, try again.");
                    break;
                }
            } catch (InputMismatchException e) {
                System.err.println("Invalid input, try again.");
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
            }
        }
    }
}
