package pkgTwo;
import pkgOne.MyInterface;

public class MyClass implements MyInterface {
    @Override
    public void methodOne() {
        System.out.println("Implementation of methodOne");
    }

    @Override
    public void methodTwo() {
        System.out.println("Implementation of methodTwo");
    }

    @Override
    public void methodThree() {
        System.out.println("Implementation of methodThree");
    }
}
