import java.util.InputMismatchException;
import java.util.Scanner;

class NegativeNumberException extends Exception {
    public NegativeNumberException(String message) {
        super(message);
    }
}

public class CustomExceptionExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("Enter an integer: ");
            int userInput = scanner.nextInt();
            if (userInput < 0) {
                throw new NegativeNumberException("Number cannot be less than zero");
            }
            System.out.println("You entered: " + userInput);
        } catch (InputMismatchException e) {
            System.out.println("Input must be an integer.");
        } catch (NegativeNumberException e) {
            System.out.println(e.getMessage());
        }
    }
}
