interface Interface1 {
    void method1();
    void method2();
}

interface Interface2 {
    void method3();
    void method4();
}

interface NewInterface extends Interface1, Interface2 {
    void newMethod();
}

class ConcreteClass {
    void concreteMethod() {
        System.out.println("Concrete method in the concrete class");
    }
}

class DerivedClass extends ConcreteClass implements NewInterface {
    @Override
    public void method1() {
        System.out.println("Implementation of method1");
    }

    @Override
    public void method2() {
        System.out.println("Implementation of method2");
    }

    @Override
    public void method3() {
        System.out.println("Implementation of method3");
    }

    @Override
    public void method4() {
        System.out.println("Implementation of method4");
    }

    @Override
    public void newMethod() {
        System.out.println("Implementation of newMethod");
    }
}

public class InterfaceExample {
    public static void main(String[] args) {
        DerivedClass obj = new DerivedClass();

        obj.method1();
        obj.method2();
        obj.method3();
        obj.method4();
        obj.newMethod();
        obj.concreteMethod();
    }
}
