import java.util.Scanner;
import java.util.InputMismatchException;

class Shape {
    // Calculates the area of a triangle.
    static double area(double a, double b, double c) {
        double s = (a + b + c) / 2.0;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }
    // Area of a rectangle.
    static int area(int length, int width) {
        return length * width;
    }
    // Area of a square.
    static int area(int sideLength) {
        return sideLength * sideLength;
    }
    // Area of a circle.
    static double area(double radius) {
        return Math.PI * radius * radius;
    }
}

class ShapeAreaCalculations {
    public static void main(String args[]) {
	try (var sc = new Scanner(System.in)) {
        System.out.println("Choose the shape: ");
        System.out.println("1. Triangle\n2. Rectangle\n3. Square\n4. Circle");
        System.out.print("Enter your choice: ");
        var choice = sc.nextInt();
        switch (choice) {
            case 1:
                System.out.print("Enter sides: ");
                var a = sc.nextDouble();
                var b = sc.nextDouble();
                var c = sc.nextDouble();
                System.out.println("Area of a triangle: " + Shape.area(a, b, c));
                break;
            case 2:
                System.out.print("Enter length and width: ");
                var len = sc.nextInt();
                var wid = sc.nextInt();
                System.out.println("Area of a rectangle: " + Shape.area(len, wid));
                break;
            case 3:
                System.out.print("Enter side length: ");
                var sidelen = sc.nextInt();
                System.out.println("Area of a square: " + Shape.area(sidelen));
                break;
            case 4:
                System.out.print("Enter radius: ");
                var r = sc.nextDouble();
                System.out.println("Area of a square: " + Shape.area(r));
                break;
            default:
                System.out.println("Invalid choice");
                break;
        }
	} catch (InputMismatchException e) {
            System.err.println("Invalid number given as input");
        }
    }
}
