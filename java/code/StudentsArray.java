import java.util.*;

class Subject {
    String title;
    int theory;

    Subject(String title, int theory) {
        this.title = title;
        this.theory = theory;
    }

    static Subject input(Scanner sc) {
        System.out.println("Enter details of subject:");
        System.out.print("Enter title: ");
        String title = sc.nextLine();
        System.out.print("Enter theory marks: ");
        int theory = sc.nextInt();
        sc.skip("\n");
        return new Subject(title, theory);
    }
    int getTheoryMarks() { return theory; }

    void display() {
        System.out.println("  Marks obtained in " + title + ": " + theory);
    }
}

class Student {
    final static int SUBJECT_COUNT = 3;
    long roll;
    String name;
    String stream;
    Subject[] subjects;

    Student(long roll, String name, String stream, Subject[] subjects) {
        this.roll = roll;
        this.name = name;
        this.stream = stream;
        this.subjects = subjects;
    }

    static Student input(Scanner sc) {
        System.out.println("Enter the details of the student:");
        System.out.print("Enter Roll no.: ");
        long roll = sc.nextLong();
        sc.skip("\n");
        System.out.print("Enter name: ");
        String name = sc.nextLine();
        System.out.print("Enter stream: ");
        String stream = sc.nextLine();
        Subject[] subjects = new Subject[SUBJECT_COUNT];
        for (int i = 0; i < subjects.length; i++) {
            System.out.print("For subject " + (i + 1) + ": ");
            subjects[i] = Subject.input(sc);
        }
        return new Student(roll, name, stream, subjects);
    }

    void display() {
        System.out.println("Student details:");
        System.out.println("  Roll No.: " + roll);
        System.out.println("  Name: " + name);
        System.out.println("  Stream: " + stream);
        int totalMarks = 0;
        for (Subject subject : subjects) {
            subject.display();
            totalMarks += subject.getTheoryMarks();
        }
        double average = ((double)totalMarks) / subjects.length;
        System.out.println("  Average percentage: " + String.format("%.2f", average) + "%");
    }
}

class StudentsArray {
    public static void main(String[] args) {
        var sc = new Scanner(System.in);
        System.out.println("Enter the number of students: ");
        Student[] students = new Student[sc.nextInt()];
        System.out.println("Enter the details of the students:");
        for (int i = 0; i < students.length; i++) {
            System.out.print("For student " + (i + 1) + ": ");
            students[i] = Student.input(sc);
        }
        System.out.println("Displaying the details of the students:");
        for (int i = 0; i < students.length; i++) {
            System.out.println("\nFor student " + (i + 1) + ": ");
            students[i].display();
        }
    }
}

