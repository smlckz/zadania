class Singleton {
    private static Singleton instance;
    private Singleton() {
        System.out.println("Creating singleton instance");
    }
    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        System.out.println("Providing singleton instance");
        return instance;
    }
}

class SingletonExample {
    public static void main(String[] args) {
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
        if (s1 == s2)
            System.out.println("The singleton instances are identical.");
    }
}

