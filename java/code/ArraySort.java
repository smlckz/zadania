import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.function.Consumer;

class Array {
    private double[] arr;
    Array() throws Exception { takeInput(); }
    void takeInput() throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the length: ");
        int size = sc.nextInt();
        if (size < 0)
            throw new Exception("Invalid length, can not be negative");
        arr = new double[size];
        System.out.print("Enter the array elements: ");
        for (int i = 0; i < size; i++)
            arr[i] = sc.nextDouble();
    }
    void display() {
        System.out.print("The array elements are:");
        for (int i = 0; i < arr.length; i++)
            System.out.print(" " + arr[i]);
        System.out.println();
    }
    double get(int i) { return arr[i]; }
    void set(int i, double val) { arr[i] = val; }
    int size() { return arr.length; }
}

class ArrayOperations {
    static void bubbleSort(Array arr) {
        int n = arr.size();
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                double x = arr.get(j), xp = arr.get(j + 1);
                if (x > xp) {
                    arr.set(j, xp);
                    arr.set(j + 1, x);
                }
            }
        }
    }
    static void selectionSort(Array arr) {
        int n = arr.size();
        for (int i = 0; i < n - 1; i++) {
            int minidx = i;
            for (int j = i + 1; j < n; j++)
                if (arr.get(j) < arr.get(minidx))
                    minidx = j;
            double tmp = arr.get(minidx);
            arr.set(minidx, arr.get(i));
            arr.set(i, tmp);
        }
    }
}

class ArraySort {
    static void menu() {
        System.out.println(
            "Menu:\n" +
            " 1. Re-enter array\n" +
            " 2. Display array elements\n" +
            " 3. Perform bubble sort\n" +
            " 4. Perform selection sort\n" +
            " 5. Exit\n");
    }
    static void performSort(Array arr, Consumer<Array> sorter) {
        System.out.print("Before sorting: ");
        arr.display();
        sorter.accept(arr);
        System.out.print("After sorting: ");
        arr.display();
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Menu-driven program of array operations\n");
        System.out.println("Enter the array:");
        Array arr = null;
        while (true) {
            try {
                if (arr == null) arr = new Array();
                menu();
                System.out.print("Enter your choice: ");
                int choice = sc.nextInt();
                switch (choice) {
                case 1:
                    arr.takeInput();
                    arr.display();
                    break;
                case 2:
                    arr.display();
                    break;
                case 3:
                    performSort(arr, ArrayOperations::bubbleSort);
                    break;
                case 4:
                    performSort(arr, ArrayOperations::selectionSort);
                    break;
                case 5:
                    System.out.println("Bye.");
                    return;
                default:
                    System.err.println("Invalid choice, try again.");
                }
            } catch (InputMismatchException e) {
                System.err.println("Error: Invalid input, try again");
            } catch (Exception e) {
                System.err.println("Error: " + e.getMessage());
            }
        }
    }
}

