class OuterClass {
   private int outerMember = 10;

   class InnerClass {
       private int innerMember = 20;

       void accessMembers() {
           System.out.println("Outer member: " + outerMember);
           System.out.println("Inner member: " + innerMember);
       }
   }

   void accessInnerMembers() {
       InnerClass inner = new InnerClass();
       System.out.println("Inner member (through object): " + inner.innerMember);
   }
}

class InnerClassExample {
   public static void main(String[] args) {
       OuterClass outer = new OuterClass();
       OuterClass.InnerClass inner = outer.new InnerClass();

       inner.accessMembers();
       outer.accessInnerMembers();
   }
}
