#import "template.typ": *
#show: body => apply(body)
#set text(size: 1.1em)
#set raw(lang: "java-new")
#let semibold(body) = text(weight: 600, body)
#text(size: 1.4em, align(center)[#semibold[Lab Assignments] \ _on_ \ #semibold[Object Oriented Programming using Java]])
#set enum(full: true, numbering: (..args) => {
  let patterns = ("1.", "a)")
  let pattern = patterns.at(calc.min(args.pos().len(), patterns.len()) - 1)
  numbering(pattern, args.pos().last())
})

+ Write a menu-driven program to implement linear and binary search on an object of a custom array class.
+ Write a menu-driven program to implement bubble and selection sort on an object of a custom array class.
+ Write a menu-driven program to create a class with a `String` member variable and perform basic string operations:
  - count the number of words,
  - reverse the string, and
  - case-insensitively check whether the string is palindrome or not.
+ Write a menu-driven program to perform addition, subtraction and multiplication operations on objects of a custom matrix class.
+ Write a program to add two numbers by taking input using command line input, the `Scanner` class and the `BufferedReader` class.
+ Write a program to find the surface area and volume of a cylinder using constructors - keyboard input or command-line input.
+ Write a program to add two complex numbers using concept of methods returning objects and methods taking objects as parameters.
+ Write a program to find a number from an array of number objects.
+ Write a program to show that `static` blocks get executed before any object creation and demonstrate the use of static variable to count the number of objects.
+ Write a program to implement a singleton class.
+ Write a program to make a student class with attributes for _roll number_, _name_ and _stream_. Assume that a student studies 3 subjects having full marks of 100 each. Each subject has a _title_ and _theory marks_. From the main class, create an array of such students. Show their specific information, along with average percentage of marks.
+ Design a class to represent a bank account. Include the following:
  #pad(left: 1em)[
  / Fields :
    - Name of the depositor
    - Address of the depositor
    - Account number
    - Balance amount in the account
  / Methods :
    - To assign initial values
    - To deposit an amount
    - To withdraw an amount after checking balance
    - To display the name, address and balance of a customer.
  ]
  From `main()` create object and call these methods.
+ Write a program to create a class `Shape` with 4 methods to calculate the areas of triangle, rectangle, square, and circle using method overloading.
+ Write a program to create an abstract class `Shape` with two abstract methods, `area()` and `display()` and make three concrete derived classes `Rectangle`, `Circle` and `Triangle` which can calculate area and display them seperately.
+ Write a program to create a class `Parent` having instance variables `id`, `name` and `address`; a class `ChildOne` having instance variables `id`, `name`, `address` and `marks`; another class `ChildTwo` with instance variables `id`, `name`, `address`, `qualification` and `salary`. Design the program using `super` call with proper parameters within each class and define your own method to display values of the member variable and use an object of each class from `main()` to display their properties.
+ Write a program to create a base class named `Rectangle` and another class named `Cuboid` deriving `Rectangle` overloading the constructors and print surface area and volume of a `Cuboid` object.
+ Write a program to create a class `Employee` with instance variables `name` and `id`; a subclass of `Employee` named `Scientist` with instance variables `no_of_publication` and `experience`; and its subclass named `DScientist` with instance variable `award`; implement the `public String toString() { }` method in each class to describe about its object with the member variables and from `main()` method create an object of each class and print each object.
+ Write a program to create a class named `CircularBase` containing a method `getArea()` to calculate the base area, an interface named `_3dShape` with two methods `calArea()` and `calVolume()`, two subclasses of `CircularBase` named `Cone` and `Cylinder` implementing `_3dShape`; calculate the base area and volume of these shapes with the help of `calArea()` method.
+ Write a program to create a class containing an inner class to show that inner class can use members of outer class directly, but the outer class can use members of inner class only through its objects. Check the name of the inner class file created when it was compiled.
+ Create two interfaces, each with two methods. Inherit a new interface from the two, adding a new method. Create a class by implementing the new interface and also inheriting a concrete class. In the `main()` method, create an object of the derived class and call these methods [do all without package statement].
+ Create an Interface. Create two sub-classes implementing the interface. Show the functionalities of runtime polymorphism / dynamic method dispatch using them.
+ Create a class with variable(s) and method(s) (all will be default accessed) under package `pOne`. Now create a class under package `pTwo`, which is subclass of firstly created class. In the method here (_i.e._ class of `pTwo`) call variable(s) and method(s) of previous class (_i.e._ class of `pOne`). If errors come, rectify them. Now from `main()` (under working directory) access members of the second class.
+ Create an interface containing three methods, in a package `pkgOne`. Implement the interface from a class under package `pkgTwo`. From `main()`, under working directory, create object of the class and call methods of interface.
+ Write a program to take an integer number from the user. If the number is less then zero then throw a custom exception. Also, catch the exception when other datatypes except integers are given. 
+ Write a program in Java to create three threads printing 1 to 100. Implement the program using both inheriting Thread class and implementing Runnable interface.

#line(length: 100%)
#colbreak()

+ Assume that a bank maintains two kinds of account for its customers, one called savings account and other called current account. The savings account provides compound interest and withdrawal facilities but no cheque book facility. The current account provides cheque book facility but no interest. Current account holders should also maintain a minimum balance (say Rs. 1000) and if the balance falls below this level a service charge is imposed (say Rs. 100). Create a class `Account` that stores customer name, account number and type of account. From this class derive two classes `Curr_Acct` and `Savn_Acct` respectively to make them more specific to their requirements. Include the necessary methods to achieve the following tasks:
  + Accept deposit from a customer and update the balance.
  + Display the balance.
  + Compute and deposit interest.
  + Permit withdrawal and update the balance.
  + Check for minimum balance, impose penalty, if necessary, and update balance.
  Use constructors to initialise the class members.

#line(length: 100%)

1. Take a string from keyboard and convert into character array (new one).
2. Take a string from keyboard and a `char` array (filled up to length 5). Now append the string to that `char` array. Show the `char` array.
3. Write a java code to differentiate `equals()` method and `==` operator.
4. Find length of a string taken from keyboard and also find the length of that string except the spaces at the beginning and the end of the string.
5. Sort ten names in ascending order.
6. Check if `"Tech"` is present in `"University of Technology"` or not. If yes return its position.
7. Take a sentence and convert it into string arrays and sort the words using any sorting technique.
8. Show that the `String` objects are immutable but `StringBuffer` objects are mutable.
9. Convert a `StringBuffer` object into a `String` object. Print the final result.
10. Check whether a given string is a palindrome or not. Ignore the cases.
11. Convert a string into an array of strings and display them [use command-line argument].
12. Take a shopping list of five items from the command line and store them in a vector.
13. Write a program to concatenate the contents of two strings.

